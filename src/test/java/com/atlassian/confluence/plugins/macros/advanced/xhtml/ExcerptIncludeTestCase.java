package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputDeviceType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultRenderer;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.links.linktypes.BlogPostLink;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.links.LinkResolver;
import junit.framework.TestCase;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.confluence.security.Permission.VIEW;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class ExcerptIncludeTestCase extends TestCase
{
    // Wording of error doesn't matter as long as the correct key is requested.
    private static final String PAGE_NOT_FOUND_ERROR = "This does not exist...";
    private static final String NO_LINK_ERROR = "No link!";

    private ExcerptIncludeMacro excerptIncludeMacro;
    private Map<String, String> parameters;

    @Mock private DefaultConversionContext conversionContext;
	@Mock private PageContext pageContext;
	@Mock private LinkResolver linkResolver;
	@Mock private PermissionManager permissionManager;
	@Mock private ExcerptHelper excerptHelper;
    @Mock private DefaultRenderer viewRenderer;
    private I18NBean i18nBean;

    @Override
	protected void setUp() throws Exception
	{
		super.setUp();

		MockitoAnnotations.initMocks(this);

		parameters = new HashMap<String, String>();

        // Most tests can run with just the body content rendered.
        parameters.put("nopanel", "true");

        when(viewRenderer.render(anyString(), (ConversionContext)anyObject())).thenAnswer(new Answer<String>() {
            public String answer(InvocationOnMock invocationOnMock) {
                return (String)invocationOnMock.getArguments()[0];
            }
        });
        when(pageContext.getOutputDeviceType()).thenReturn(ConversionContextOutputDeviceType.DESKTOP);
        when(conversionContext.getPageContext()).thenReturn(pageContext);

        I18NBeanFactory i18nBeanFactory = mock(I18NBeanFactory.class);
        LocaleManager localeManager = mock(LocaleManager.class);
        i18nBean = mock(I18NBean.class);
        when(i18nBean.getText(eq("excerptinclude.error.page-does-not-exists"), any(Object[].class))).thenReturn(PAGE_NOT_FOUND_ERROR);
        when(i18nBean.getText(eq("excerptinclude.error.cannot-link-to"), any(Object[].class))).thenReturn(NO_LINK_ERROR);
        when(i18nBeanFactory.getI18NBean(null)).thenReturn(i18nBean);

        excerptIncludeMacro = new ExcerptIncludeMacro();
        excerptIncludeMacro.setViewRenderer(viewRenderer);
        excerptIncludeMacro.setLinkResolver(linkResolver);
        excerptIncludeMacro.setPermissionManager(permissionManager);
        excerptIncludeMacro.setExcerptHelper(excerptHelper);
        excerptIncludeMacro.setI18NBeanFactory(i18nBeanFactory);
        excerptIncludeMacro.setLocaleManager(localeManager);
    }

	public void testWithValidPage() throws MacroExecutionException, ParseException
    {
		String excerpt = "This is excerpt text";

		parameters.put("0", "Testpage");

        Page page = new Page();
        PageLink link = mock(PageLink.class);
        when(link.getDestinationContent()).thenReturn(page);
        when(linkResolver.createLink(pageContext, "Testpage")).thenReturn(link);
		when(permissionManager.hasPermission(null, VIEW, page)).thenReturn(true);
		when(excerptHelper.getExcerpt(page)).thenReturn(excerpt);

		assertEquals("This is excerpt text", executeMacroAndGetBodyContent());
	}

    public void testWithUnpermittedPage() throws MacroExecutionException, ParseException
    {
        String excerpt = "This is excerpt text";

        parameters.put("0", "Testpage");

        Page page = new Page();
        PageLink link = mock(PageLink.class);
        when(link.getDestinationContent()).thenReturn(page);
        when(linkResolver.createLink(pageContext, "Testpage")).thenReturn(link);
        when(permissionManager.hasPermission(null, VIEW, page)).thenReturn(false);  // just making this clear
        when(excerptHelper.getExcerpt(page)).thenReturn(excerpt);

        assertEquals(PAGE_NOT_FOUND_ERROR, executeMacroAndGetBodyContent());
    }

    public void testWithPageTitleParameter() throws MacroExecutionException
    {
        String excerpt = "This is excerpt text";

        parameters.put("pageTitle", "Testpage");

        Page page = new Page();
        PageLink link = mock(PageLink.class);
        when(link.getDestinationContent()).thenReturn(page);
        when(linkResolver.createLink(pageContext, "Testpage")).thenReturn(link);
        when(permissionManager.hasPermission(null, VIEW, page)).thenReturn(true);
        when(excerptHelper.getExcerpt(page)).thenReturn(excerpt);

        assertEquals("This is excerpt text", executeMacroAndGetBodyContent());
    }

    public void testGoodBlogPostReference() throws MacroExecutionException
    {
        String excerpt = "This is excerpt text";
        final String title = "Test BlogPost";

        parameters.put("blogPost", "2009/10/01/Test BlogPost");

        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(title);

        BlogPostLink link = mock(BlogPostLink.class);
        when(link.getDestinationContent()).thenReturn(blogPost);
        when(linkResolver.createLink(pageContext, "2009/10/01/Test BlogPost")).thenReturn(link);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), VIEW, blogPost)).thenReturn(true);
        when(excerptHelper.getExcerpt(blogPost)).thenReturn(excerpt);

        assertEquals("This is excerpt text", executeMacroAndGetBodyContent());
    }

    public void testBadBlogPostReference() throws MacroExecutionException
	{
		parameters.put("blogPost", "tst:/2009/10/01/This is a non-existent post");

        BlogPostLink link = mock(BlogPostLink.class);
        when(link.getPageTitle()).thenReturn("This is a non-existent post");
        when(link.getDestinationContent()).thenReturn(null);   // i.e. not found
        when(linkResolver.createLink(pageContext, "tst:/2009/10/01/This is a non-existent post")).thenReturn(link);

        Document document = executeWithPanelAndGetDocument();
        assertEquals("This is a non-existent post", document.getElementsByClass("panelHeader").get(0).text());
        assertEquals(PAGE_NOT_FOUND_ERROR, document.getElementsByClass("panelContent").get(0).text());
        verify(linkResolver).createLink(pageContext, "tst:/2009/10/01/This is a non-existent post");
	}

    public void testWithoutContentEntityObject() throws MacroExecutionException
	{
        assertEquals(NO_LINK_ERROR, executeMacroAndGetBodyContent());
    }

	public void testBlogPostPanelHeader() throws MacroExecutionException
    {
	    String blogTitle = "Testing Blog Title";
        String linkText = "tst:2009/12/20/" + blogTitle;
        parameters.put("blogPost", linkText);

        BlogPostLink link = mock(BlogPostLink.class);
        when(link.getPageTitle()).thenReturn(blogTitle);
        when(linkResolver.createLink(pageContext, linkText)).thenReturn(link);

        Document doc = executeWithPanelAndGetDocument();
        String renderedTitle = doc.getElementsByClass("panelHeader").get(0).text();
	    assertEquals(blogTitle, renderedTitle);
	}

    public void testBlogPostPanelHeaderWithNoSpaceKey() throws MacroExecutionException
    {
        String blogTitle = "Testing Blog Title";
        String linkText = "/2009/12/20/" + blogTitle;
        parameters.put("blogPost", linkText);

        BlogPostLink link = mock(BlogPostLink.class);
        when(link.getPageTitle()).thenReturn(blogTitle);
        when(linkResolver.createLink(pageContext, linkText)).thenReturn(link);

        Document doc = executeWithPanelAndGetDocument();
        String renderedTitle = doc.getElementsByClass("panelHeader").get(0).text();
        assertEquals(blogTitle, renderedTitle);
    }

    public void testBlockInfiniteIncludes() throws Exception
    {
        Page page = new Page();
        page.setTitle("Testpage");
        parameters.put("pageTitle", "Testpage");

        PageLink link = mock(PageLink.class);
        when(link.getDestinationContent()).thenReturn(page);
        when(linkResolver.createLink(pageContext, "Testpage")).thenReturn(link);
        when(permissionManager.hasPermission(null, VIEW, page)).thenReturn(true);

        String recursionError = "Oh no, recursion! recursion! recursion!";
        when(i18nBean.getText("excerptinclude.error.recursive.message")).thenReturn(recursionError);

        try
        {
            // If the page is already on the stack, then we've already started rendering that page, which includes
            // some other content which includes the {excerpt-include} macro which includes the page again. That could
            // turn out badly - instead we return a message.
            ContentIncludeStack.push(page);
            String body = executeMacroAndGetBodyContent();
            assertTrue("Body is: " + body, body.contains(recursionError));
        }
        finally
        {
            ContentIncludeStack.pop();
        }
    }

    private String executeMacroAndGetBodyContent() throws MacroExecutionException
    {
        return excerptIncludeMacro.execute(parameters, "", conversionContext);
    }

    private org.jsoup.nodes.Document executeWithPanelAndGetDocument() throws MacroExecutionException
    {
        parameters.put("nopanel", "false");

        String html = executeMacroAndGetBodyContent();
        return Jsoup.parseBodyFragment(html);
    }
}

