package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartHandlePaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartIndexPaginationParameters;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.ResultFilter;
import com.atlassian.confluence.search.v2.filter.HandleBasedSubsetResultFilter;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;

public class DefaultRecentChangesSearchBuilderTestCase extends AbstractTestCase
{
    private RecentChangesSearchBuilder recentChangesSearchBuilder;
    private QueryParameters nullQueryParams;

    public void testSearchIsConfiguredToRetrieveOneMoreThanPageSize()
    {
        ISearch search = recentChangesSearchBuilder.getChangesSearch(nullQueryParams, new StartIndexPaginationParameters(0));

        final ResultFilter resultFilter = search.getResultFilter();

        assertTrue(resultFilter instanceof SubsetResultFilter);
        assertEquals(PaginationParameters.DEFAULT_SIZE + 1, ((SubsetResultFilter) resultFilter).getMaxResults());
    }

    public void testSearchIsConfiguredToRetrieveOneMoreThanPageSizeForStartHandleParameter() throws Exception
    {
        ISearch search = recentChangesSearchBuilder.getChangesSearch(nullQueryParams, new StartHandlePaginationParameters(new HibernateHandle(Page.class.getName() + "-123")));

        final ResultFilter resultFilter = search.getResultFilter();

        assertTrue(resultFilter instanceof HandleBasedSubsetResultFilter);
        assertEquals(PaginationParameters.DEFAULT_SIZE + 1, ((HandleBasedSubsetResultFilter) resultFilter).getPageSize());
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        recentChangesSearchBuilder = new DefaultRecentChangesSearchBuilder();
        nullQueryParams = new QueryParameters(null, null, null, null);
    }
}
