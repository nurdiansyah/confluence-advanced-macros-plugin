package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartHandlePaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartIndexPaginationParameters;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.RequestCacheThreadLocal;

import java.util.Collections;

public class ChangesUrlTestCase extends AbstractTestCase
{
    public void testGetWithStartHandlePagination()
    {
        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, "/confluence"));

        final String authors = "fred,harry";
        final String labels = "-foo,+bar";
        final String spaceKeys = "JIRA,CONF";
        final QueryParameters queryParams = new QueryParameters(labels, authors, Page.CONTENT_TYPE, spaceKeys);
        final HibernateHandle startHandle = new HibernateHandle(Page.class.getName(), 123);
        final PaginationParameters paginationParams = new StartHandlePaginationParameters(startHandle);

        final Theme theme = Theme.social;
        ChangesUrl url = new ChangesUrl(queryParams, paginationParams, theme);
        final String changesUrl = url.get();

        assertTrue("startHandle not found in url: " + changesUrl, changesUrl.contains("startHandle=" + startHandle.toString()));
        assertTrue(changesUrl.contains("pageSize=" + paginationParams.getPageSize()));
        assertTrue(changesUrl.contains("theme=" + theme));
        assertTrue(changesUrl.contains("authors=" + GeneralUtil.urlEncode(authors)));
        assertTrue(changesUrl.contains("labels=" + GeneralUtil.urlEncode(labels)));
        assertTrue(changesUrl.contains("spaceKeys=" + GeneralUtil.urlEncode(spaceKeys)));
        assertTrue(changesUrl.contains("contentType=" + Page.CONTENT_TYPE));
    }

    public void testGetWithStartIndexPagination()
    {
        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, "/confluence"));

        final QueryParameters queryParams = new QueryParameters(null, null, null, null);
        final int startIndex = 5;
        final PaginationParameters paginationParams = new StartIndexPaginationParameters(startIndex);

        final Theme theme = Theme.social;
        ChangesUrl url = new ChangesUrl(queryParams, paginationParams, theme);
        final String changesUrl = url.get();

        assertTrue("startIndex not found in url: " + changesUrl, changesUrl.contains("startIndex=" + startIndex));
        assertTrue(changesUrl.contains("pageSize=" + paginationParams.getPageSize()));
    }

    public void testGetWorksWithNullContextPath()
    {
        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, null));

        assertEquals("/plugins/recently-updated/changes.action?theme=social", new ChangesUrl(null, null, Theme.social).get());
    }
}
