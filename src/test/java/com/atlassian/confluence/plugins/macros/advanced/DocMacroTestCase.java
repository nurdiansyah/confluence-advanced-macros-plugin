package com.atlassian.confluence.plugins.macros.advanced;

import java.util.Map;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DocMacroTestCase extends TestCase
{
	private Map parameters;
	private RenderContext renderContext;
	private String relativeLink = "browse";
	private DocMacro docMacro;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		parameters = mock(Map.class);
		renderContext = mock(RenderContext.class);
		
		docMacro = new DocMacro();
	}
	
	public void testWhetherBodyTextIsRenderedAsHtml() throws MacroException
	{
		String body = "{info} <u>Hi</u> <font color=#339933>this</font> is <b>info macro</b> ";
		String urlLink = "<a href=\"http://confluence.atlassian.com/" + relativeLink + "\" target=\"_blank\">" + body + "</a>";
		
		when(parameters.get("0")).thenReturn(relativeLink);
		
		assertEquals(urlLink, docMacro.execute(parameters, body, renderContext));
	}
	
	public void testIsInline()
	{
		assertEquals(true, docMacro.isInline());
	}
	
	public void testHasBody()
	{
		assertEquals(true, docMacro.hasBody());
	}
	
	public void testBodyRenderMode()
	{
	    assertEquals(RenderMode.INLINE.or(RenderMode.allow(RenderMode.F_MACROS)), docMacro.getBodyRenderMode());
	}
	
	public void testSuppressSurroundingTagDuringWysiwygRenderingBooleanValue()
	{
	    assertEquals(true, docMacro.suppressSurroundingTagDuringWysiwygRendering());
	}
	
	public void testSuppressMacroRenderingDuringWysiwyg()
	{
	    assertEquals(false, docMacro.suppressMacroRenderingDuringWysiwyg());
	}
}
