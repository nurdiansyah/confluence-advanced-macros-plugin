package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.spring.container.ContainerManager;
import junit.framework.TestCase;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract test case that sets up mockito mocks and a ContainerManager for unit tests.
 * <p>
 * Subclasses can override {@link #getAdditionalSingletons()} to set up any additional singletons that need to be accessible via
 * {@link com.atlassian.spring.container.ContainerManager#getComponent(String)}.
 */
public abstract class AbstractTestCase extends TestCase
{
    @Mock protected I18NBean i18n;
    @Mock protected SettingsManager settingsManager;
    protected Settings settings;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        final Map<String, Object> singletons = new HashMap<String, Object>();
        singletons.put("settingsManager", settingsManager);
        singletons.putAll(getAdditionalSingletons());

        ContainerManagerInitializer containerManagerInitializer = new ContainerManagerInitializer(singletons);
        containerManagerInitializer.init();

        settings = new Settings();
        when(settingsManager.getGlobalSettings()).thenReturn(settings); 
    }

    @Override
    protected void tearDown() throws Exception
    {
        ContainerManager.resetInstance();
        i18n = null;
        settings = null;
        settingsManager = null; 

        super.tearDown();
    }

    protected Map<String, ?> getAdditionalSingletons()
    {
        return Collections.emptyMap();
    }
}
