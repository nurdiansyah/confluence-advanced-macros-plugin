package com.atlassian.confluence.plugins.macros.advanced;

import junit.framework.TestCase;

import static org.mockito.Mockito.*;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;

public class NavigationMapMacroTestCase extends TestCase
{
    private LabelManager labelManager;

    private NavigationMapMacro navigationMapMacro;

    private Map<String, Object> macroVelocityContext;

    private Map<String, String> macroParams;

    private Page pageToRenderOn;

    private String labelOne = "labelone";
    
    private Label labelOneObject = new Label(labelOne);

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        labelManager = mock(LabelManager.class);

        macroVelocityContext = new HashMap<String, Object>();
        macroParams = new HashMap<String, String>();
        
        navigationMapMacro = new TestNavigationMapMacro();

        pageToRenderOn = new Page();
        labelOne = "labelone";
        labelOneObject = new Label(labelOne);
    }

    public void testMacroExceptionRaisedIfDefaultParameterNotSpecifed()
    {
        try
        {
            navigationMapMacro.execute(macroParams, null, pageToRenderOn.toPageContext());
            fail();
        }
        catch (MacroException me)
        {

        }
    }

    public void testOnlyPageWithLabelProcessed() throws MacroException
    {
        final String fakeOutput = "fakeOutput";
        final Page pageWithLabel = new Page();
        final BlogPost blogPostWithLabel = new BlogPost();

        when(labelManager.getLabel(labelOne)).thenReturn(labelOneObject);
        when(labelManager.getContent(labelOneObject)).thenReturn(
                new ArrayList(Arrays.asList(
                        pageWithLabel, blogPostWithLabel
                ))
        );

        macroParams.put("0", labelOne);

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(Arrays.asList(pageWithLabel), contextMap.get("pages"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testPagesWithTitleSortedByTitle() throws MacroException
    {
        final String fakeOutput = "fakeOutput";
        final Page pageWithLabel = new Page();
        final Page pageWithLabel2 = new Page();
        final Page pageWithLabel3 = new Page();

        pageWithLabel.setTitle("C");
        pageWithLabel2.setTitle("B");
        pageWithLabel3.setTitle("A");

        when(labelManager.getLabel(labelOne)).thenReturn(labelOneObject);
        when(labelManager.getContent(labelOneObject)).thenReturn(
                new ArrayList(Arrays.asList(
                        pageWithLabel, pageWithLabel2, pageWithLabel3
                ))
        );

        macroParams.put("0", labelOne);

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(Arrays.asList(pageWithLabel3, pageWithLabel2, pageWithLabel), contextMap.get("pages"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testNoPagesReturnedIfLabelDoesNotExist() throws MacroException
    {
        final String fakeOutput = "fakeOutput";
        
        macroParams.put("0", labelOne);

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(Collections.emptyList(), contextMap.get("pages"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testWrapAfterParameterDefaultsToFive() throws MacroException
    {
        final String fakeOutput = "fakeOutput";
        final Page pageWithLabel = new Page();

        when(labelManager.getLabel(labelOne)).thenReturn(labelOneObject);
        when(labelManager.getContent(labelOneObject)).thenReturn(new ArrayList(Arrays.asList(pageWithLabel)));

        macroParams.put("0", labelOne);

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(5, contextMap.get("wrapAfter"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testCustomWrapAfterParameterRespected() throws MacroException
    {
        final String fakeOutput = "fakeOutput";
        final Page pageWithLabel = new Page();

        when(labelManager.getLabel(labelOne)).thenReturn(labelOneObject);
        when(labelManager.getContent(labelOneObject)).thenReturn(new ArrayList(Arrays.asList(pageWithLabel)));

        macroParams.put("0", labelOne);
        macroParams.put("wrapAfter", String.valueOf(10));

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(10, contextMap.get("wrapAfter"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testCellWidthDefaultsToNinety() throws MacroException
    {
        final String fakeOutput = "fakeOutput";

        macroParams.put("0", labelOne);

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(90, contextMap.get("cellWidth"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testCustomCellWidthRespected() throws MacroException
    {
        final String fakeOutput = "fakeOutput";

        macroParams.put("0", labelOne);
        macroParams.put("cellWidth", String.valueOf(100));

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(100, contextMap.get("cellWidth"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testCellHeightDefaultsToSixty() throws MacroException
    {
        final String fakeOutput = "fakeOutput";

        macroParams.put("0", labelOne);

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(60, contextMap.get("cellHeight"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testCustomCellHeightRespected() throws MacroException
    {
        final String fakeOutput = "fakeOutput";

        macroParams.put("0", labelOne);
        macroParams.put("cellHeight", String.valueOf(100));

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals(100, contextMap.get("cellHeight"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testTitleParameterRespected() throws MacroException
    {
        final String fakeOutput = "fakeOutput";

        macroParams.put("0", labelOne);
        macroParams.put("title", "title");

        assertEquals(fakeOutput,
                new TestNavigationMapMacro()
                {
                    @Override
                    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
                    {
                        assertEquals("title", contextMap.get("title"));
                        return fakeOutput;
                    }
                }.execute(macroParams, null, pageToRenderOn.toPageContext())); 
    }

    public void testRendersBlock()
    {
        assertFalse(navigationMapMacro.isInline());
    }

    public void testHasNoBody()
    {
        assertFalse(navigationMapMacro.hasBody());
    }

    public void testDoesNotRenderBody()
    {
        assertEquals(RenderMode.NO_RENDER, navigationMapMacro.getBodyRenderMode());
    }

    private class TestNavigationMapMacro extends NavigationMapMacro
    {
        private TestNavigationMapMacro()
        {
            setLabelManager(labelManager);
        }

        @Override
        protected Map<String, Object> getMacroVelocityContext()
        {
            return macroVelocityContext;
        }
    }
}
