package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

import junit.framework.TestCase;

import static java.util.Arrays.asList;
import java.util.List;

public class PageTestCase extends TestCase
{
    public void testItemsMoreThanPageSize()
    {
        final List<String> items = asList("foo", "bar", "baz");
        Page<String> page = new Page<String>(items, 2);

        assertEquals(items.subList(0, 2), page.getItems());
    }

    public void testItemsLessThanPageSize()
    {
        final List<String> items = asList("foo");
        Page<String> page = new Page<String>(items, 2);

        assertEquals(items, page.getItems()); 
    }
}
