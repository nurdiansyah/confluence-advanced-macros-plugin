package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.thumbnail.ThumbnailInfo;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.ThumbnailInfoFactory;
import com.atlassian.spring.container.ContainerContext;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AttachmentUpdateItemTestCase extends AbstractTestCase
{
    private AttachmentUpdateItem attachmentUpdateItem;
    private String contextPath = "/confluence";
    
    @Mock private SearchResult searchResult;
    @Mock private DateFormatter dateFormatter;
    @Mock private AnyTypeDao anyTypeDao;
    @Mock private Handle handle;
    @Mock private Attachment attachment;
    @Mock private ThumbnailInfoFactory thumbnailInfoFactory;
    @Mock private ThumbnailInfo thumbnailInfo;
    @Mock private ContainerContext containerContext;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, contextPath));

        when(searchResult.getHandle()).thenReturn(handle);
        when(anyTypeDao.findByHandle(handle)).thenReturn(attachment);
        
        attachmentUpdateItem = new AttachmentUpdateItem(searchResult, dateFormatter, i18n, "");
    }

    @Override
    protected Map<String, ?> getAdditionalSingletons()
    {
        Map<String, Object> singletons = new HashMap<String, Object>();
        singletons.put("anyTypeDao", anyTypeDao);
        singletons.put("thumbnailInfoFactory", thumbnailInfoFactory);

        return singletons;
    }

    public void testGetBodyReturnsNullWhenAttachmentCannotBeThumbnailed()
    {
        when(thumbnailInfoFactory.getThumbnailInfo(attachment)).thenReturn(null);

        assertNull(attachmentUpdateItem.getBody());
    }
    
/*
    public void testGetAttachmentHtmlBodyWithAttachmentDownloadPathThumbnailUrlPathWidthAndHeight()
    {
        String attachmentDownloadPath = "/download/FishTank.zip";
        String thumbnailUrlPath = "/img/clock.jpeg";
        Integer thumbnailWidth = 500;
        Integer thumbnailHeight = 450;
        
        when(thumbnailInfoFactory.getThumbnailInfo(attachment)).thenReturn(thumbnailInfo);
        when(attachment.getDownloadPathWithoutVersion()).thenReturn(attachmentDownloadPath);
        when(thumbnailInfo.getThumbnailUrlPath()).thenReturn(thumbnailUrlPath);
        when(thumbnailInfo.getThumbnailWidth()).thenReturn(thumbnailWidth);
        when(thumbnailInfo.getThumbnailHeight()).thenReturn(thumbnailHeight);
        
        assertEquals("<a href=\"" + contextPath + attachmentDownloadPath + "\">"+
                "<img border=\"0\" class=\"thumbnail\" src=\"" + thumbnailUrlPath + "\" " +
                "width=\"" + thumbnailWidth + "\" height=\"" + thumbnailHeight + "\"></a>", 
                attachmentUpdateItem.getBody());
    }
*/
}
