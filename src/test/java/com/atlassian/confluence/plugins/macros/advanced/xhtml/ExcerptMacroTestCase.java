package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.PageContext;

public class ExcerptMacroTestCase extends TestCase
{
    @Mock private DefaultConversionContext conversionContext;
    @Mock private PageContext pageContext;

    private ExcerptMacro excerptMacro;

    private Map<String, String> macroParams;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        excerptMacro = new ExcerptMacro();
        macroParams = new HashMap<String, String>();
        when(conversionContext.getRenderContext()).thenReturn(pageContext);
    }

    public void testShowExcerpt() throws MacroExecutionException
    {
        String body = "body";

        assertEquals(body, excerptMacro.execute(macroParams, body, conversionContext));
    }

    public void testHideExcerpt() throws MacroExecutionException
    {
        String body = "body";

        macroParams.put("hidden", Boolean.TRUE.toString());

        assertEquals("", excerptMacro.execute(macroParams, body, conversionContext));
    }

    public void testGetBodyType()
    {
        assertEquals(Macro.BodyType.RICH_TEXT, excerptMacro.getBodyType());
    }

    public void testGetOutputType()
    {
        assertEquals(Macro.OutputType.INLINE, excerptMacro.getOutputType());
    }
    
    public void testStripParagraph()
    {
        Matcher matcher = ExcerptMacro.stripPattern.matcher("<p>some nested p's<p>nested</p>asdfas</p>");
        assertTrue(matcher.matches());
        assertEquals("some nested p's<p>nested</p>asdfas", matcher.group(1));
        
        matcher = ExcerptMacro.stripPattern.matcher("<p>some start</p>with trailing text");
        assertFalse(matcher.matches());
        
        matcher = ExcerptMacro.stripPattern.matcher("<p>some text</p>");
        assertTrue(matcher.matches());
        assertEquals("some text", matcher.group(1));
        
        matcher = ExcerptMacro.stripPattern.matcher("starting text<p>some start</p>");
        assertFalse(matcher.matches());
        
        matcher = ExcerptMacro.stripPattern.matcher("<p>First paragraph</p><p>second para</p>");
        assertTrue(matcher.matches());
        assertEquals(matcher.group(1), "First paragraph</p><p>second para");
    }
}
