package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.ResultFilterFactory;
import static com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters.MAX_PAGE_SIZE;
import com.atlassian.confluence.search.v2.ResultFilter;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import junit.framework.TestCase;

public class StartIndexPaginationParametersTestCase extends TestCase
{
    public void testGetOneMoreThanPageSize()
    {
        ResultFilterFactory params = new StartIndexPaginationParameters(0, 5);

        final ResultFilter resultFilter = params.getResultFilter();
        assertTrue(resultFilter instanceof SubsetResultFilter);
        assertEquals(6, ((SubsetResultFilter) resultFilter).getMaxResults());
    }

    public void testMaxResultsLimitedAgainstAbuse()
    {
        ResultFilterFactory params = new StartIndexPaginationParameters(0, 99999);

        final ResultFilter resultFilter = params.getResultFilter();
        assertTrue(resultFilter instanceof SubsetResultFilter);
        assertEquals(MAX_PAGE_SIZE, ((SubsetResultFilter) resultFilter).getMaxResults());
    }

    public void testDoNotFetchOneMoreThanPageSizeWhenPageSizeIsMax()
    {
        ResultFilterFactory params = new StartIndexPaginationParameters(0, MAX_PAGE_SIZE);

        final ResultFilter resultFilter = params.getResultFilter();
        assertTrue(resultFilter instanceof SubsetResultFilter);
        assertEquals(MAX_PAGE_SIZE, ((SubsetResultFilter) resultFilter).getMaxResults());
    }
}
