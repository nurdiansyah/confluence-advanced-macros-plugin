package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.ResultFilterFactory;
import com.atlassian.confluence.search.v2.ResultFilter;
import com.atlassian.confluence.search.v2.filter.HandleBasedSubsetResultFilter;
import junit.framework.TestCase;

public class StartHandlePaginationParametersTestCase extends TestCase
{
    private HibernateHandle handle;

    public void testGetOneMoreThanPageSize()
    {
        ResultFilterFactory params = new StartHandlePaginationParameters(handle, 5);

        final ResultFilter resultFilter = params.getResultFilter();
        assertTrue(resultFilter instanceof HandleBasedSubsetResultFilter);
        assertEquals(6, ((HandleBasedSubsetResultFilter) resultFilter).getPageSize());
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        handle = new HibernateHandle(Page.class.getName(), 123);
    }
}
