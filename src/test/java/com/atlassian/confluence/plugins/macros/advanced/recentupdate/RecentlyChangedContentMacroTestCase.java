package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import junit.framework.TestCase;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecentlyChangedContentMacroTestCase extends TestCase
{
    private TestRecentlyChangedContentMacro recentlyChangedContentMacro;

    private Page pageToRenderOn;

    private Map<String, String> macroParams;

    private Settings globalSettings;

    private Map<String, Object> macroVelocityContext;

    @Mock
    private ContainerContext containerContext;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private SearchManager searchManager;

    @Mock
    private SearchResults searchResults;

    @Mock
    private UserAccessor userAccessor;

    @Mock
    private ConfluenceUserPreferences confluenceUserPreferences;

    @Mock
    private FormatSettingsManager formatSettingsManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        ContainerManager.getInstance().setContainerContext(containerContext);
        when(containerContext.getComponent("settingsManager")).thenReturn(settingsManager);

        globalSettings = new Settings();
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        when(i18NBeanFactory.getI18NBean()).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );

        when(formatSettingsManager.getDateFormat()).thenReturn("dd/MM/yyyy");
        when(formatSettingsManager.getDateTimeFormat()).thenReturn("dd/MM/yyyy HH:mm");
        when(formatSettingsManager.getTimeFormat()).thenReturn("HH:mm");

        when(confluenceUserPreferences.getTimeZone()).thenReturn(TimeZone.getDefault());

        macroVelocityContext = new HashMap<String, Object>();

        recentlyChangedContentMacro = new TestRecentlyChangedContentMacro();

        recentlyChangedContentMacro.setI18NBeanFactory(i18NBeanFactory);
        recentlyChangedContentMacro.setLocaleManager(localeManager);
        recentlyChangedContentMacro.setSearchManager(searchManager);
        recentlyChangedContentMacro.setUserAccessor(userAccessor);
        recentlyChangedContentMacro.setFormatSettingsManager(formatSettingsManager);

        pageToRenderOn = new Page();
        pageToRenderOn.setSpace(new Space("tst"));
        pageToRenderOn.setTitle("Test Page");

        macroParams = new HashMap<String, String>();

    }

    @Override
    protected void tearDown() throws Exception
    {
        formatSettingsManager = null;
        confluenceUserPreferences = null;
        userAccessor = null;
        searchResults = null;
        searchManager = null;
        i18NBean = null;
        localeManager = null;
        i18NBeanFactory = null;
        settingsManager = null;
        containerContext = null;
        macroVelocityContext = null;
        globalSettings = null;
        macroParams = null;
        pageToRenderOn = null;
        recentlyChangedContentMacro = null;

        ContainerManager.getInstance().setContainerContext(null);
        super.tearDown();
    }

    public void testContentTypeParamNotModifiedInExecute() throws MacroException, InvalidSearchException
    {
        try
        {
            when(searchManager.search((ISearch) anyObject())).thenReturn(searchResults);
            when(userAccessor.getConfluenceUserPreferences((User) anyObject())).thenReturn(confluenceUserPreferences);
            ConfluenceUser user = mock(ConfluenceUser.class);
            AuthenticatedUserThreadLocal.set(user);
            final Locale locale = Locale.ENGLISH;
            when(localeManager.getLocale(user)).thenReturn(locale);
            when(i18NBeanFactory.getI18NBean(locale)).thenReturn(i18NBean);

            assertEquals("", recentlyChangedContentMacro.execute(macroParams, null, pageToRenderOn.toPageContext()));

            // This is the closest I can get to verifying that the content type param is initalized only once.
            // Every time it initializes, it will find out if shared mode is enabled from the global settings.
            // So if shared mode is only querief for once, we're good.
            verify(settingsManager, times(1)).getGlobalSettings();
        }
        finally
        {
            AuthenticatedUserThreadLocal.set(null);
        }
    }

    private class TestRecentlyChangedContentMacro extends RecentlyChangedContentMacro
    {
        @Override
        Map<String, Object> getMacroVelocityContext()
        {
            return macroVelocityContext;
        }

        @Override
        String renderRecentlyUpdated(Theme theme, Map<String, Object> macroRenderContext)
        {
            return "";
        }
    }
}
