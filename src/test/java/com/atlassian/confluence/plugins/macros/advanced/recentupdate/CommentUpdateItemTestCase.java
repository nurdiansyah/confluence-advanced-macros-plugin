package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchResult;
import org.apache.commons.lang.RandomStringUtils;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

public class CommentUpdateItemTestCase extends AbstractTestCase
{
    private CommentUpdateItem commentUpdateItem;

    @Mock private SearchResult searchResult;
    @Mock private DateFormatter dateFormatter;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        commentUpdateItem = new CommentUpdateItem(searchResult, dateFormatter, i18n, "");
    }
    
    public void testRePrefixDroppedFromTitle()
    {
        final String title = "Foo Bar";
        when(searchResult.getDisplayTitle()).thenReturn("Re: " + title);
        assertEquals(title, commentUpdateItem.getUpdateTargetTitle());
    }

    public void testWikiMarkupStrippedFromBody()
    {
        when(searchResult.getContent()).thenReturn("*foo* bar");

        assertEquals("foo bar", commentUpdateItem.getBody());
    }

    public void testContentTitlesAreEscaped()
    {
        final String title = "<script>alert('XSS')</script>";
        when(searchResult.getDisplayTitle()).thenReturn("Re: " + title);
        assertEquals("&lt;script&gt;alert(&#39;XSS&#39;)&lt;/script&gt;", commentUpdateItem.getUpdateTargetTitle());
    }

    public void testLongCommentsAreTruncated()
    {
        final String originalCommentBody = RandomStringUtils.randomAlphabetic(130);
        when(searchResult.getContent()).thenReturn(originalCommentBody);

        assertTrue(commentUpdateItem.getBody().length() < originalCommentBody.length());
    }
}
