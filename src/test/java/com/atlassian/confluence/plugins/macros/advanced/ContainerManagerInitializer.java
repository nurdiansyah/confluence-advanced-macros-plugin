package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.languages.DefaultLocaleManager;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.spring.container.SpringTestContainerContext;
import com.atlassian.user.User;
import org.springframework.context.support.StaticApplicationContext;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

/**
 * Initializes the {@link ContainerManager} for tests
 */
public class ContainerManagerInitializer
{
    private final Map<String, ?> additionalSingletons;

    public ContainerManagerInitializer()
    {
        this.additionalSingletons = Collections.emptyMap();
    }

    public ContainerManagerInitializer(Map<String, ?> additionalSingletons)
    {
        this.additionalSingletons = additionalSingletons;
    }

    public void init()
    {
        StaticApplicationContext testContext = new StaticApplicationContext();

        for (Map.Entry<String, ?> entry : additionalSingletons.entrySet())
            testContext.getBeanFactory().registerSingleton(entry.getKey(), entry.getValue());

        testContext.getBeanFactory().registerSingleton("localeManager", new DefaultLocaleManager()
        {
            public Locale getLocale(User user)
            {
                return Locale.ENGLISH;
            }
        });

        testContext.refresh();
        ContainerManager.getInstance().setContainerContext(new SpringTestContainerContext(testContext));
    }
}
