package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import static com.atlassian.confluence.search.lucene.DocumentFieldName.CONTENT_VERSION;
import static com.atlassian.confluence.search.lucene.DocumentFieldName.LATEST_VERSION_ID;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.spaces.SpaceDescription;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ContentUpdateItemTestCase extends AbstractTestCase
{
    @Mock private SearchResult searchResult;
    @Mock private DateFormatter dateFormatter;

    private ContentUpdateItem contentUpdateItem;

    public void testChangesLinkForTwoValidVersions()
    {
        Map<String, String> extraFields = new HashMap<String, String>(2);
        extraFields.put(CONTENT_VERSION, "2");
        extraFields.put(LATEST_VERSION_ID, "123");
        when(searchResult.getExtraFields()).thenReturn(extraFields);
        when(searchResult.getType()).thenReturn("page");

        String changesLink = contentUpdateItem.getChangesLink();

        assertTrue("changesLink: " + changesLink, changesLink.contains("selectedPageVersions=2"));
        assertTrue("changesLink: " + changesLink, changesLink.contains("selectedPageVersions=1"));
        assertTrue("changesLink: " + changesLink, changesLink.contains("pageId=123"));
    }

    public void testChangesLinkIsNullIfLatestVersionIdIsMissing()
    {
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(CONTENT_VERSION, "2"));

        assertNull(contentUpdateItem.getChangesLink());
    }

    public void testChangesLinkIsNullIfContentVersionLessThanOne()
    {
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(CONTENT_VERSION, "0"));

        assertNull(contentUpdateItem.getChangesLink());
    }

    public void testChangesLinkIsNullIfContentVersionIsMissing()
    {
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(LATEST_VERSION_ID, "123"));

        assertNull(contentUpdateItem.getChangesLink());
    }

    public void testChangesLinkIsNullIfVersionIsNotANumber()
    {
        Map<String, String> extraFields = new HashMap<String, String>(2);
        extraFields.put(CONTENT_VERSION, "abc");
        extraFields.put(LATEST_VERSION_ID, "123");
        when(searchResult.getExtraFields()).thenReturn(extraFields);

        assertNull(contentUpdateItem.getChangesLink());
    }

    public void testGetDescriptionForNewContentForFirstVersionsOfContent()
    {
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(CONTENT_VERSION, "1"));

        assertEquals("update.item.desc.author.created", contentUpdateItem.getDescriptionAndAuthorKey());
    }

    public void testGetDescriptionForUpdatedContentForNonFirstVersionsOfContent()
    {
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(CONTENT_VERSION, "3"));

        assertEquals("update.item.desc.author.updated", contentUpdateItem.getDescriptionAndAuthorKey());
    }

    public void testNoChangesLinkWhenContentUpdateItemIsNotPageOrBlog() throws Exception {
        //expect that there is no diff url when the update is a space description
        when(searchResult.getType()).thenReturn(SpaceDescription.CONTENT_TYPE_PERSONAL_SPACEDESC);
        assertNull("There shouldn't be any changes link when the content type is a personal space description", contentUpdateItem.getChangesLink());
        when(searchResult.getType()).thenReturn(SpaceDescription.CONTENT_TYPE_SPACEDESC);
        assertNull("There shouldn't be any changes link when the content type is a space description", contentUpdateItem.getChangesLink());
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        contentUpdateItem = new ContentUpdateItem(searchResult, dateFormatter, i18n, "");
    }
}
