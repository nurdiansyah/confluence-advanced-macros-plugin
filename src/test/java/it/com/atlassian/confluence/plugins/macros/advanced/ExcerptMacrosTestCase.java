package it.com.atlassian.confluence.plugins.macros.advanced;

import org.apache.commons.lang.time.FastDateFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExcerptMacrosTestCase extends AbstractConfluencePluginWebTestCaseBase
{
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("/yyyy/MM/dd/");
    
    public void testExcerptHidden()
    {
        viewPageById(
                createPage(
                        TEST_SPACE_KEY,
                        "testExcerptHidden",
                        "{excerpt:hidden=true}Hidden excerpt{excerpt}"
                )
        );

        assertTextNotPresent("Hidden excerpt");
    }

    public void testExcerptNotHiddenByDefault()                                                                                                                                   
    {
        viewPageById(
                createPage(
                        TEST_SPACE_KEY,
                        "testExcerptNotHiddenByDefault",
                        "{excerpt:hidden=true}Excerpt text{excerpt}"
                )
        );

        assertTextNotPresent("Excerpt text");
    }

    public void testIncludeExcerptFromBlogPost()
    {
        Date blogPostPublishedDate = new Date();

        String blogTitle = "blogPostWithExcerpt";
        String excerptText = "Excerpt text";

        long blogPostId = createBlogPost(TEST_SPACE_KEY, blogTitle, "{excerpt}" + excerptText + "{excerpt}", blogPostPublishedDate);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromBlogPost", "{excerpt-include:blogPost=" + FastDateFormat.getInstance("yyyy/MM/dd/").format(blogPostPublishedDate) + blogTitle + "}");

        viewPageById(blogPostId); /* Seems like excerpt is only stored on macro render? */
        viewPageById(testPageId);

        assertEquals(blogTitle, getElementTextByXPath("//div[@class='wiki-content']/div[@class='panel']/div[@class='panelHeader']"));
        assertEquals(excerptText, getElementTextByXPath("//div[@class='wiki-content']/div[@class='panel']/div[@class='panelContent']"));
    }

    public void testIncludeExcerptFromPage()
    {
        String pageWithExcerptTitle = "pageWithExcerpt";
        String excerptText = "Excerpt text";

        long pageWithExcerptId = createPage(TEST_SPACE_KEY, pageWithExcerptTitle, "{excerpt}" + excerptText + "{excerpt}");
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromPage", "{excerpt-include:" + pageWithExcerptTitle + "}");

        viewPageById(pageWithExcerptId); /* Seems like excerpt is only stored on macro render? */
        viewPageById(testPageId);

        assertEquals(pageWithExcerptTitle, getElementTextByXPath("//div[@class='wiki-content']/div[@class='panel']/div[@class='panelHeader']"));
        assertEquals(excerptText, getElementTextByXPath("//div[@class='wiki-content']/div[@class='panel']/div[@class='panelContent']"));
    }

	public void testIncludeExcerptWithNoPanel()
    {
        String pageWithExcerptTitle = "pageWithExcerpt";
        String excerptText = "Excerpt text";

        long pageWithExcerptId = createPage(TEST_SPACE_KEY, pageWithExcerptTitle, "{excerpt}" + excerptText + "{excerpt}");
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromPage", "{excerpt-include:" + pageWithExcerptTitle + "|nopanel=true}");

        viewPageById(pageWithExcerptId); /* Seems like excerpt is only stored on macro render? */
        viewPageById(testPageId);

        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='panel']");
        assertEquals(excerptText, getElementTextByXPath("//div[@class='wiki-content']"));
    }

	public void testCircularReference()
	{
		long pageD = createPage(TEST_SPACE_KEY, "D", "{excerpt}apples{excerpt-include:blogPost=" + getBlogpostWikiValue("E") +"|nopanel=true}{excerpt}");
		long blogPostE = createBlogPost(TEST_SPACE_KEY, "E", "{excerpt}bananas{excerpt-include:F|nopanel=true}{excerpt}");
		long pageF = createPage(TEST_SPACE_KEY, "F", "{excerpt}pears{excerpt-include:D|nopanel=true}{excerpt}");

		viewPageById(pageD);
		assertTextsPresentInOrder(new String[]{"apples", "bananas", "pears", "apples"});
		assertTextPresent("Unable to render {excerpt-include}. Already included page E, stopping.");
		viewPageById(blogPostE);
        assertTextsPresentInOrder(new String[]{"bananas", "pears", "apples", "bananas"});
		assertTextPresent("Unable to render {excerpt-include}. Already included page F, stopping.");
		viewPageById(pageF);
        assertTextsPresentInOrder(new String[]{"pears", "apples", "bananas", "pears"});
		assertTextPresent("Unable to render {excerpt-include}. Already included page D, stopping.");
	}

	public void testCircularReferenceWithPanels()
	{
		long pageD = createPage(TEST_SPACE_KEY, "D", "{excerpt}D{excerpt-include:blogPost=" + getBlogpostWikiValue("E") +"}{excerpt}");
		long blogPostE = createBlogPost(TEST_SPACE_KEY, "E", "{excerpt}E{excerpt-include:F}{excerpt}");
		long pageF = createPage(TEST_SPACE_KEY, "F", "{excerpt}F{excerpt-include:D}{excerpt}");

		viewPageById(pageD);
		assertTextPresent("Unable to render {excerpt-include}. Already included page E, stopping.");
		viewPageById(blogPostE);
		assertTextPresent("Unable to render {excerpt-include}. Already included page F, stopping.");
		viewPageById(pageF);
		assertTextPresent("Unable to render {excerpt-include}. Already included page D, stopping.");
	}

	public void testBothIncludeAndExcerptInclude()
	{
		long pageD = createPage(TEST_SPACE_KEY, "D", "{excerpt}D{excerpt-include:E}{excerpt}\n{include:E}");
		createPage(TEST_SPACE_KEY, "E", "{excerpt}A short excerpt{excerpt}\nSome text");

		viewPageById(pageD);
		assertTextPresent("D");
        assertTextPresent("A short excerpt");
		assertTextPresent("Some text");
		assertTextNotPresent("Unable to render {excerpt-include}.");
	}

	private String getBlogpostWikiValue(String title)
	{
		return DATE_FORMAT.format(new Date()) + "/" + title;
	}
}
