package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;

import java.util.Date;

import com.atlassian.confluence.plugin.functest.helper.UserHelper;

public class ChangeHistoryMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    public void editPage(long pageId, String content, String changeComment)
    {
        updatePage(pageId, content);
    }

    private void assertTableHeading(int headingIndex, String headingText)
    {
        assertEquals(
                headingText,
                getElementTextByXPath("//div[@class='wiki-content']//table//tr/th[" + (headingIndex + 1) +"]")
        );
    }

    private void assertVersionRow(
            int rowIndex,
            String versionNumber,
            String date,
            String editor,
            String editComment)
    {
        boolean isCurrentVersionRow = rowIndex == 1;

        if (isCurrentVersionRow)
        {
            assertEquals(
                    "Current Version (v. " + versionNumber + ")",
                    getElementTextByXPath("//div[@class='wiki-content']//table//tr[" + (rowIndex + 1) + "]/td")
            );
        }


        assertEquals(
                isCurrentVersionRow ? "Current Version" : "v. " + versionNumber,
                getElementTextByXPath("//div[@class='wiki-content']//table//tr[" + (rowIndex + 1) + "]/td"  + (isCurrentVersionRow ? "/b" : StringUtils.EMPTY) + "/a")
        );
        assertEquals(
                date,
                getElementTextByXPath("//div[@class='wiki-content']//table//tr[" + (rowIndex + 1) + "]/td[2]" + (isCurrentVersionRow ? "/b" : StringUtils.EMPTY))
        );

        assertEquals(
                editor,
                getElementTextByXPath("//div[@class='wiki-content']//table//tr[" + (rowIndex + 1) + "]/td[3]/b/a")
        );
/* comment back in when the func-test framework is updated to allow a version comment to be saved when editing a page
        assertEquals(
                editor + (StringUtils.isBlank(editComment) ? StringUtils.EMPTY : " : " + editComment),
                getElementTextByXPath("//div[@class='wiki-content']//table//tr[" + (rowIndex + 1) + "]/td[3]")
        );
*/
    }

    public void testRenderPageHistoryWithEditCommentsFromVariousUsers()
    {
        long pageToEditId = createPage(TEST_SPACE_KEY,
                "testRenderPageHistoryWithEditCommentsFromVariousUsers",
                StringUtils.EMPTY);
        String date = FastDateFormat.getInstance("MMM dd, yyyy HH:mm").format(new Date());

        String otherUserName = "otheruser";
        createUser(otherUserName, otherUserName, otherUserName, new String[] { "confluence-users", "confluence-administrators" });

        editPage(pageToEditId, "Edited by " + getConfluenceWebTester().getCurrentUserName(), "Edit comment 1");
        String date1 = FastDateFormat.getInstance("MMM dd, yyyy HH:mm").format(new Date());

        logout();
        login(otherUserName, otherUserName);
        editPage(pageToEditId, "Edited by " + getConfluenceWebTester().getCurrentUserName(), "Edit comment 2");
        String date2 = FastDateFormat.getInstance("MMM dd, yyyy HH:mm").format(new Date());

        logout();
        loginAsAdmin();
        editPage(pageToEditId, "Edited by " + getConfluenceWebTester().getCurrentUserName(), "Edit comment 3");
        String date3 = FastDateFormat.getInstance("MMM dd, yyyy HH:mm").format(new Date());

        logout();
        login(otherUserName, otherUserName);
        editPage(pageToEditId, "{change-history}", "Edit comment 4");
        String date4 = FastDateFormat.getInstance("MMM dd, yyyy HH:mm").format(new Date());

        viewPageById(pageToEditId);

        assertTableHeading(0, "Version");
        assertTableHeading(1, "Date");
        assertTableHeading(2, "Comment");

        UserHelper userHelper = getUserHelper(getConfluenceWebTester().getAdminUserName());

        assertTrue(userHelper.read());

        String adminFullName = userHelper.getFullName();

        assertVersionRow(1, "5", date4, otherUserName, "Edit comment 4");
        assertVersionRow(2, "4", date3, adminFullName, "Edit comment 3");
        assertVersionRow(3, "3", date2, otherUserName, "Edit comment 2");
        assertVersionRow(4, "2", date1, adminFullName, "Edit comment 1");
        assertVersionRow(5, "1", date, adminFullName, "");
    }
}
