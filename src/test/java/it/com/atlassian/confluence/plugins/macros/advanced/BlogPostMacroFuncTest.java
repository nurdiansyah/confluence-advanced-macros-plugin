package it.com.atlassian.confluence.plugins.macros.advanced;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.xmlrpc.XmlRpcException;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.SpacePermission;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

public class BlogPostMacroFuncTest extends AbstractConfluencePluginWebTestCaseBase {


    public static final String TEST_SPACE2_TITLE = "Test Space 2"; //getTestProperty("testspace.title");
    public static final String TEST_SPACE2_KEY = "tst2";	//getTestProperty("testspace2.key");
    public static final String TEST_SPACE2_DESCRIPTION = "Test Space 2";	//getTestProperty("testspace2.description");
    
    public static final String TEST_SPACE3_TITLE = "Test Space 3"; //getTestProperty("testspace.title");
    public static final String TEST_SPACE3_KEY = "tst3"; //getTestProperty("testspace3.key");
    public static final String TEST_SPACE3_DESCRIPTION = "Test Space 3"; //getTestProperty("testspace3.description");
    
    public static final String TESTPAGE_TITLE = "Test Page"; //getTestProperty("testpage.title");

    public static final String BLOGPOST_XPATH = "//div[@class='wiki-content']//div[@class='blog-post-list']//ul/li[@class='blog-item']";

    private static final String TEST_GEN_USERNAME1 = "user_one";
    private static final String TEST_GEN_PASSWORD1 = "password_one";

	/**
     * The length of time to pause between actions must be longer for some databases than others.  Specifically, we
     * know that we store dates in 1 second granularity in Oracle.
     */
    protected static final int PAUSE_MILLIS = 10;	//getTestPropertyAsInt("pause.time.millis", 10);
    protected static final int PAUSE_MYSQL_MILLIS = 1000; // MySQL datetime type has a 1 second granularity

    
    /**
     * Add three blog posts
     * Create a page with blog-posts macro with max set to 2
     * The later two blog posts should display
     * Also tests for resolution to recursion problem
     */
    public void testBlogPostMacro() throws Exception
    {
        createBlogPost(TEST_SPACE_KEY, "First Post", "First Post Content");
        // if the two posts are created too close together, they come back in the wrong order
        pause(PAUSE_MILLIS);
        createBlogPost(TEST_SPACE_KEY, "Second Post", "Second Post Content");
        createBlogPost(TEST_SPACE_KEY, "Blog macro page", "{blog-posts:2}");

        getIndexHelper().update();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        viewBlog(TEST_SPACE_KEY, dateFormat.format(new Date()), "Blog macro page");
        
        assertTitleEquals("Blog macro page", TEST_SPACE_TITLE);
        assertTextNotPresent("First Post Content");
        assertTextPresent("Second Post Content");
        assertTextPresent("Already included page Blog macro page, stopping.");
    }
    
    
    public void testBlogPostMacroOrderedByCreationDate() throws Exception
    {
        long firstBlogId = createBlogPost(TEST_SPACE_KEY, "First Post", "First Post Content");
        // if the two posts are created too close together, they come back in the wrong order
        pause(PAUSE_MILLIS);
        createBlogPost(TEST_SPACE_KEY, "Second Post", "Second Post Content");
        pause(PAUSE_MYSQL_MILLIS);
        editBlogPost(firstBlogId, "First Post", "New First Post Content");

        long pageId = createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:2}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
        
        assertTextsPresentInOrder(new String[] { "Second Post", "First Post" });
    }


    public void testBlogPostMacroWithNoPosts() throws IOException, XmlRpcException
    {
        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts}");

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
        assertTextPresent("Blog stream");
        assertTextPresent("Create a blog post to share news and announcements with your team and company.");
        assertLinkPresentWithText("Create blog post");
    }

    //CONFDEV-17399: new blank experience for blog post macro
    public void testBlogPostMacroWithNoPostsAndNoCreateBlogPermission() throws IOException, XmlRpcException
    {
        final ConfluenceRpc rpc = ConfluenceRpc.newInstance(getConfluenceWebTester().getBaseUrl(), ConfluenceRpc.Version.V2_WITH_WIKI_MARKUP);
        
        User admin = new User("admin", "admin", "admin", "admin@atlassian.com");
        rpc.logIn(admin);
        
        Space space = new Space(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);
        rpc.createSpace(space);
        
        User user = new User(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1, TEST_GEN_USERNAME1, TEST_GEN_USERNAME1 + "@atlassian.com");
        rpc.createUser(user);
        
        Page page = new Page(space, TESTPAGE_TITLE, "{blog-posts}");
        rpc.createPage(page);

        rpc.grantPermission(SpacePermission.VIEW, TEST_SPACE2_KEY, user);
        logout();
        
        login(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1);
        viewPage(TEST_SPACE2_KEY, TESTPAGE_TITLE);
        
        assertTextPresent("Blog stream");
        assertTextPresent("Create a blog post to share news and announcements with your team and company.");
        assertLinkNotPresentWithText("Create blog post");

        logout();
        loginAsAdmin();
        deleteUser(TEST_GEN_USERNAME1);
    }

    public void testBlogPostMacroInSecondSpace() throws IOException, XmlRpcException
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        // Create the second space
        createSpace(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);

        // Add blog posts to the original space
        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        // Create a test page in the second space to show blog posts
        createPage(TEST_SPACE2_KEY, TESTPAGE_TITLE, "{blog-posts}");

        viewPage(TEST_SPACE2_KEY, TESTPAGE_TITLE);

        // Make sure no content is displayed
        assertTextPresent("Blog stream");
        assertTextPresent("Create a blog post to share news and announcements with your team and company.");
        assertLinkPresentWithText("Create blog post");
        assertLinkNotPresentWithText(postTitleOne);
        assertTextNotPresent(postContentOne);
        assertLinkNotPresentWithText(postTitleTwo);
        assertTextNotPresent(postContentTwo);
        
        deleteSpace(TEST_SPACE2_KEY);
    }

    public void testBlogPostMacroInBlogPost() throws Exception
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        String postTitleThree = "title 3";
        String postContentThree = "{blog-posts}";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);
        long blogPostIdThree = createBlogPost(TEST_SPACE_KEY, postTitleThree, postContentThree);
        pause(PAUSE_MYSQL_MILLIS);
        
        getIndexHelper().update();

        viewPageById(blogPostIdThree);

        // Check that only content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
    }

    public void testBlogPostMacroInBlogPostInPage() throws Exception
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        String postTitleThree = "title 3";
        String postContentThree = "{blog-posts}";

        String pageTitle = "paaage 1";
        String pageContent = "{blog-posts}";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);
        createBlogPost(TEST_SPACE_KEY, postTitleThree, postContentThree);

        // Create the page
        createPage(TEST_SPACE_KEY, pageTitle, pageContent);

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, pageTitle);

        // Check that only content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
        assertLinkPresentWithText(postTitleThree);
    }

    public void testBlogPostMacroWithTitles() throws Exception
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:content=titles}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // Check that only content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertTextNotPresent(postContentOne);
        assertLinkPresentWithText(postTitleTwo);
        assertTextNotPresent(postContentTwo);
    }

    public void testBlogPostMacroWithTitle() throws Exception
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:content=title}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // Check that only content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertTextNotPresent(postContentOne);
        assertLinkPresentWithText(postTitleTwo);
        assertTextNotPresent(postContentTwo);
    }

    public void testBlogPostMacroWithMaxResultOne() throws Exception
    {
        String postTitleOne = "First Blog title";
        String postContentOne = "content 1";

        String postTitleTwo = "Second Blog title";
        String postContentTwo = "content 2";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        pause(100);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:1}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // Check that only content is displayed
        assertLinkNotPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
    }

    public void testBlogPostMacroWithSeparateMaxParameter() throws Exception
    {
        String postTitleOne = "First Blog title";
        String postContentOne = "content 1";

        String postTitleTwo = "Second Blog title";
        String postContentTwo = "content 2";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        pause(100);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:max=1}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // Check that only content is displayed
        assertLinkNotPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
    }
    
    public void testBlogPostMacroWithSperateMaxResultParameter() throws Exception
    {
    	String postTitleOne = "First Blog title";
        String postContentOne = "content 1";

        String postTitleTwo = "Second Blog title";
        String postContentTwo = "content 2";

        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        pause(100);
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:maxResults=1}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // Check that only content is displayed
        assertLinkNotPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
    }

    public void testBlogPostSortByTitle()
    {
        String blog1 = "blog1";
        String blog2 = "blog2";
        String blog3 = "blog3";

        createBlogPost(TEST_SPACE_KEY, blog3, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog1, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog2, StringUtils.EMPTY, new Date());

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:sort=title|content=title}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        assertEquals(blog1, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blog2, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertEquals(blog3, getElementTextByXPath(BLOGPOST_XPATH + "[3]//span[@class='blog-title']/a"));
    }

    public void testBlogPostSortByTitleReversed()
    {
        String blog1 = "blog1";
        String blog2 = "blog2";
        String blog3 = "blog3";

        createBlogPost(TEST_SPACE_KEY, blog3, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog1, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog2, StringUtils.EMPTY, new Date());

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:sort=title|reverse=true|content=title}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        assertEquals(blog3, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blog2, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertEquals(blog1, getElementTextByXPath(BLOGPOST_XPATH + "[3]//span[@class='blog-title']/a"));
    }

    private void updateBlogPost(long blogId)
    {
        BlogPostHelper helper = getBlogPostHelper(blogId);

        assertTrue(helper.read());
        helper.setContent("\nEdited by " + getConfluenceWebTester().getCurrentUserName());
        assertTrue(helper.update());
    }

    public void testBlogPostSortByModificationDate()
    {
        String blog1 = "blog1";
        String blog2 = "blog2";
        String blog3 = "blog3";

        long blog1Id = createBlogPost(TEST_SPACE_KEY, blog1, StringUtils.EMPTY, new Date());
        long blog2Id = createBlogPost(TEST_SPACE_KEY, blog2, StringUtils.EMPTY, new Date());
        long blog3Id = createBlogPost(TEST_SPACE_KEY, blog3, StringUtils.EMPTY, new Date());

        updateBlogPost(blog3Id);
        updateBlogPost(blog2Id);
        updateBlogPost(blog1Id);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:sort=modified|content=title}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        assertEquals(blog3, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blog2, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertEquals(blog1, getElementTextByXPath(BLOGPOST_XPATH + "[3]//span[@class='blog-title']/a"));
    }

    public void testBlogPostSortByModificationDateReversed()
    {
        String blog1 = "blog1";
        String blog2 = "blog2";
        String blog3 = "blog3";

        long blog1Id = createBlogPost(TEST_SPACE_KEY, blog1, StringUtils.EMPTY, new Date());
        long blog2Id = createBlogPost(TEST_SPACE_KEY, blog2, StringUtils.EMPTY, new Date());
        long blog3Id = createBlogPost(TEST_SPACE_KEY, blog3, StringUtils.EMPTY, new Date());

        updateBlogPost(blog3Id);
        updateBlogPost(blog2Id);
        updateBlogPost(blog1Id);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:sort=modified|reverse=true|content=title}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        assertEquals(blog1, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blog2, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertEquals(blog3, getElementTextByXPath(BLOGPOST_XPATH + "[3]//span[@class='blog-title']/a"));
    }

    public void testBlogPostSortByCreationDate()
    {
        String blog1 = "blog1";
        String blog2 = "blog2";
        String blog3 = "blog3";

        createBlogPost(TEST_SPACE_KEY, blog1, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog2, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog3, StringUtils.EMPTY, new Date());

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:sort=modified|content=title}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        assertEquals(blog1, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blog2, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertEquals(blog3, getElementTextByXPath(BLOGPOST_XPATH + "[3]//span[@class='blog-title']/a"));
    }

    public void testBlogPostSortByCreationDateReversed()
    {
        String blog1 = "blog1";
        String blog2 = "blog2";
        String blog3 = "blog3";

        createBlogPost(TEST_SPACE_KEY, blog1, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog2, StringUtils.EMPTY, new Date());
        createBlogPost(TEST_SPACE_KEY, blog3, StringUtils.EMPTY, new Date());

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:sort=modified|reverse=true|content=title}");

        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        assertEquals(blog3, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blog2, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertEquals(blog1, getElementTextByXPath(BLOGPOST_XPATH + "[3]//span[@class='blog-title']/a"));
    }

    // Also test for ADVMACROS-129
    public void testBlogPostMacroByAuthor() throws Exception
    {
    	String postTitleOne = "First Blog title";
        String postContentOne = "content 1";
        
        String postTitleTwo = "Second Blog title";
        String postContentTwo = "content 2";

        String postTitleThree = "Third Blog title";
        String postContentThree = "content 3";

        createUser("philip", "philip", "philip", new String[] { "confluence-administrators" });
        createUser("bob", "bob", "bob", new String[] {"confluence-administrators"});


        // admin creates post 1
        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);

        logout();
        login("bob", "bob");
        // bob creates post 2
        createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);
        logout();
        
        logout();
        login("philip", "philip");
        // phillip creates post 3
        createBlogPost(TEST_SPACE_KEY, postTitleThree, postContentThree);
        logout();


        loginAsAdmin();

        // TEST single author
        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:author=philip}");
        
        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
        assertLinkNotPresentWithText(postTitleOne);
        assertLinkNotPresentWithText(postTitleTwo);
        assertLinkPresentWithText(postTitleThree);


        // Test multiple author
        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE + "1", "{blog-posts:author=philip,bob}");
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE + "1");
        assertLinkNotPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
        assertLinkPresentWithText(postTitleThree);
        
        deleteUser("philip");
        deleteUser("bob");
    }
    
    // -- Disabling this test for now until we have search API support to fix it properly.
    /**
     * Anti-test for <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-78">ADVMACROS-78</a>
     */
    //public void testBlogPostMacroTimeParameter() throws Exception
    //{
    //	Calendar now = Calendar.getInstance();
    //
    //	String postTitleOne = "First Blog title";
    //	String postContentOne = "content 1";
    //	Date postDateOne = now.getTime();
    //
    //	String postTitleTwo = "Second Blog title";
    //	String postContentTwo = "content 2";
    //	now.add(Calendar.DAY_OF_WEEK, - 1);
    //	Date postDateTwo = now.getTime();
    //
    //	String postTitleThree = "Third Blog title";
    //	String postContentThree = "content 3";
    //	now.add(Calendar.DAY_OF_MONTH, - 1);
    //	Date postDateThree = now.getTime();
    //
    //	createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne, postDateOne);
    //	pause(100);
    //	createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo, postDateTwo);
    //	pause(100);
    //	createBlogPost(TEST_SPACE_KEY, postTitleThree, postContentThree, postDateThree);
    //
    //	createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:time=3d}");
    //
    //	// Flush the queue so the smart list manager can refresh
    //    getIndexHelper().update();
    //
    //    viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
    //
    //    SimpleDateFormat formatter = new SimpleDateFormat("MMMMM dd, yyyy");
    //
    //    assertLinkPresentWithText(formatter.format(postDateOne));
    //    assertLinkPresentWithText(postTitleOne);
    //    assertTextPresent(postContentOne);
    //
    //    assertLinkPresentWithText(formatter.format(postDateTwo));
    //    assertLinkPresentWithText(postTitleTwo);
    //    assertTextPresent(postContentTwo);
    //
    //    assertLinkNotPresentWithText(formatter.format(postDateThree));
    //    assertLinkNotPresentWithText(postTitleThree);
    //    assertTextNotPresent(postContentThree);
    //}
    
    public void testBlogPostMacroGetBlogBySingleSpace() throws Exception
    {
    	String postTitleOne = "First Blog title";
    	String postContentOne = "content 1";
    	
    	String postTitleTwo = "Second Blog title";
    	String postContentTwo = "content 2";
    	
    	String postTitleThree = "Third Blog title";
    	String postContentThree = "content 3";
    	
    	// Create the second space
        createSpace(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);
    	
    	createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
    	pause(100);
    	createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);
    	pause(100);
    	
    	createBlogPost(TEST_SPACE2_KEY, postTitleThree, postContentThree);
    	
    	createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:space=" + TEST_SPACE_KEY + "}");
    	
    	// Flush the queue so the smart list manager can refresh
        getIndexHelper().update();
        
        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
        
        assertLinkPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
        assertLinkNotPresentWithText(postTitleThree);
        
        deleteSpace(TEST_SPACE2_KEY);
    }
    
    public void testBlogPostMacroGetBlogByMultipleSpaces() throws Exception
    {
    	String postTitleOne = "First Blog title";
    	String postContentOne = "content 1";
    	
    	String postTitleTwo = "Second Blog title";
    	String postContentTwo = "content 2";
    	
    	String postTitleThree = "Third Blog title";
    	String postContentThree = "content 3";
    	
    	// Create the second space
        createSpace(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);
        
        // Create the third space
        createSpace(TEST_SPACE3_KEY, TEST_SPACE3_TITLE, TEST_SPACE3_DESCRIPTION);
        
        createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        pause(100);
    	createBlogPost(TEST_SPACE2_KEY, postTitleTwo, postContentTwo);
    	pause(100);
    	createBlogPost(TEST_SPACE3_KEY, postTitleThree, postContentThree);
        
    	createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:spaces=" + TEST_SPACE_KEY + "," + TEST_SPACE3_KEY + "}");
    	
        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();
        
        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
        
        assertLinkPresentWithText(postTitleOne);
        assertLinkNotPresentWithText(postTitleTwo);
        assertLinkPresentWithText(postTitleThree);
        
        deleteSpace(TEST_SPACE2_KEY);
        deleteSpace(TEST_SPACE3_KEY);
    }
    
    public void testBlogPostMacroWithAnyLabels() throws Exception
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        String labelOne = "salami";
        String labelTwo = "cheese";

        long blogPostIdOne = createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        long blogPostIdTwo = createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        addLabelToBlogPost(labelOne, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:labels=" + labelOne + "," + labelTwo + "}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // "Excerpts" mode is the default, so check that the content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertLinkPresentWithText(postTitleTwo);
    }
    
    public void testBlogPostMacroWithAnyLabelsAndSelectBlogWithCriteria() throws Exception
    {
    	String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        String labelOne = "salami";
        String labelTwo = "cheese";

        long blogPostIdOne = createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        long blogPostIdTwo = createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        addLabelToBlogPost(labelOne, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:labels=+" + labelOne + ",+" + labelTwo + "}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // "Excerpts" mode is the default, so check that the content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertLinkNotPresentWithText(postTitleTwo);
    }

    public void testBlogPostMacroWeirdCrossSpaceIncludeBug() throws Exception
    {
        // Add blog posts to the original space
        createBlogPost(TEST_SPACE_KEY, "A Post", "some post content");
        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts}");

        // Create the second space
        createSpace(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);

        // Create a test page in the second space to show blog posts
        createPage(TEST_SPACE2_KEY, TESTPAGE_TITLE, "{include:" + TEST_SPACE_KEY + ":" + TESTPAGE_TITLE + "}");

        getIndexHelper().update();
        viewPage(TEST_SPACE2_KEY, TESTPAGE_TITLE);

        // Make sure no content is displayed
        assertTextNotPresent("Already included page " + TESTPAGE_TITLE );
        
        deleteSpace(TEST_SPACE2_KEY);
    }

    public void testInvalidValueForMatchLabelsThrowsException()
    {
        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:match-labels=wrong}");
        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // make sure the error displays on the page

        assertTextPresent("'match-labels' parameter must be set to 'all' or 'any'");
    }

    public void testBlogPostMacroWithAllLabels() throws Exception
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        String labelOne = "salami";
        String labelTwo = "cheese";

        long blogPostIdOne = createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        long blogPostIdTwo = createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        addLabelToBlogPost(labelOne, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:labels=" + labelOne + "," + labelTwo + "|match-labels=all}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // "Excerpts" mode is the default, so check that the content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertLinkNotPresentWithText(postTitleTwo);
    }

    // Test for CONF-4961
    public void testBlogPostMacroWithAllLabelsLimitOne()
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String postTitleTwo = "title 2";
        String postContentTwo = "content 2";

        String labelOne = "salami";
        String labelTwo = "cheese";

        long blogPostIdOne = createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);
        long blogPostIdTwo = createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo);

        addLabelToBlogPost(labelOne, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdOne);
        addLabelToBlogPost(labelTwo, blogPostIdTwo);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:1|labels=" + labelOne + "," + labelTwo + "|match-labels=all}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // "Excerpts" mode is the default, so check that the content is displayed
        assertLinkPresentWithText(postTitleOne);
        assertLinkNotPresentWithText(postTitleTwo);
    }

    // Test for CONF-9302
    public void testBlogPostMacroWithAllLabelsWhenLabelDoesntExist()
    {
        String postTitleOne = "title 1";
        String postContentOne = "content 1";

        String labelOne = "salami";
        String labelTwo = "cheese";

        long blogPostIdOne = createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne);

        addLabelToBlogPost(labelOne, blogPostIdOne);

        createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts:1|labels=" + labelOne + "," + labelTwo + "|match-labels=all}");

        // Flush the queue so the smart list manager can refresh
        getIndexHelper().update();

        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);

        // "Excerpts" mode is the default, so check that the content is displayed
        assertTextPresent("'cheese' is not an existing label");
        assertLinkNotPresentWithText(postTitleOne);
        assertTextNotPresent(postContentOne);
    }

    // Test for ADVMACROS-28 -- case in associated issue CONF-6166
    public void testBlogPostMacroDisplayAllNewsWithoutTimeLimit() throws Exception
    {
        final Calendar now = Calendar.getInstance();

        /* Create lots of blogs */
        for (int i = 0; i < 30; ++i)
        {
            createBlogPost(TEST_SPACE_KEY,  "Blog " + i, StringUtils.EMPTY, now.getTime());
            now.add(Calendar.DAY_OF_MONTH, -1);
        }

        final long testPageId = createPage(TEST_SPACE_KEY, "testBlogPostMacroDisplayAllNewsWithoutTimeLimit", "{blog-posts:25|content=title}");

        getIndexHelper().update();

        viewPageById(testPageId);

        assertElementPresentByXPath(BLOGPOST_XPATH + "[5]");
        assertElementPresentByXPath(BLOGPOST_XPATH + "[15]");
        assertElementPresentByXPath(BLOGPOST_XPATH + "[25]");
        assertElementNotPresentByXPath(BLOGPOST_XPATH + "[26]");
    }

    // Test for ADVMACROS-28 -- case in associated issue CONF-6166
    public void testBlogPostMacroDisplayAllNewsTitlesWithoutTimeLimit() throws Exception
    {
        final Calendar now = Calendar.getInstance();

        /* Create lots of blogs */
        for (int i = 0; i < 30; ++i)
        {
            createBlogPost(TEST_SPACE_KEY,  "Blog " + i, StringUtils.EMPTY, now.getTime());
            now.add(Calendar.DAY_OF_MONTH, -2); // going back two days at a time to vary it
        }

        final long testPageId = createPage(TEST_SPACE_KEY, "testBlogPostMacroDisplayAllNewsTitlesWithoutTimeLimit", "{blog-posts:25|content=titles}");

        getIndexHelper().update();

        viewPageById(testPageId);

        assertLinkPresentWithText("Blog " + 24);
        assertLinkNotPresentWithText("Blog " + 25);
    }

    // Test for ADVMACROS-28
    public void testBlogPostMacroDisplayAllNewsWithLabelsWithoutTimeLimit() throws Exception
    {
        final String label1 = "labelone";
        final String label2 = "labeltwo";
        final Calendar now = Calendar.getInstance();

        /* Create lots of blogs */
        for (int i = 0; i < 30; ++i)
        {
            long blogId = createBlogPost(TEST_SPACE_KEY,  "Blog " + i, StringUtils.EMPTY, now.getTime());
            addLabelToBlogPost(label1, blogId);
            addLabelToBlogPost(label2, blogId);
            now.add(Calendar.DAY_OF_MONTH, -1);
        }

        final long testPageId = createPage(TEST_SPACE_KEY, "testBlogPostMacroDisplayAllNewsWithLabelsWithoutTimeLimit", "{blog-posts:30|time=30d|match-labels=all|labels=labelone,labeltwo|content=title}");

        getIndexHelper().update();

        viewPageById(testPageId);

        assertElementPresentByXPath(BLOGPOST_XPATH + "[30]");
        assertElementNotPresentByXPath(BLOGPOST_XPATH + "[31]");
    }
    
    // Test for CONF-13117
    public void testBlogPostMacroToCheckForDuplicatedBlog() throws Exception
    {
    	Calendar now = Calendar.getInstance();
    	
    	String postTitleOne = "First Blog title";
    	String postContentOne = "content 1";
    	Date postDateOne = now.getTime();
    	
    	String postTitleTwo = "Second Blog title";
    	String postContentTwo = "content 2";
    	now.add(Calendar.DAY_OF_WEEK, - 1);
    	Date postDateTwo = now.getTime();
    	
    	createBlogPost(TEST_SPACE_KEY, postTitleOne, postContentOne, postDateOne);
    	pause(100);
    	createBlogPost(TEST_SPACE_KEY, postTitleTwo, postContentTwo, postDateTwo);
    	
    	createPage(TEST_SPACE_KEY, TESTPAGE_TITLE, "{blog-posts}");
    	
    	// Flush the queue so the smart list manager can refresh
        getIndexHelper().update();
        
        viewPage(TEST_SPACE_KEY, TESTPAGE_TITLE);
        
    	assertLinkPresentWithText(postTitleOne, 0);
    	assertLinkNotPresentWithText(postTitleOne, 1);
    	assertLinkPresentWithText(postTitleTwo, 0);
    	assertLinkNotPresentWithText(postTitleTwo, 1);
    }

    private void viewBlog(String spaceKey, String datePath, String title)
    {
        gotoPage("display/" + spaceKey + "/" + datePath + "/" + title);
        assertTitleEquals(title, TEST_SPACE_TITLE);
    }

    // TODOXHTML CONFDEV-1224
    // TODOXHTML CONFDEV-426
    public void TODOXHTML_testInvalidSpaceKeysReported()
    {
        final String invalidSpaceKey = "foobar";
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testInvalidSpaceKeysReported");
        pageHelper.setContent("{blog-posts:spaces=" + invalidSpaceKey +"}");

        assertTrue(pageHelper.create());

        getIndexHelper().update();

        viewPageById(pageHelper.getId());
        assertEquals(
                "blog-posts: '" + invalidSpaceKey +"' is not an existing space's key",
                getElementTextByXPath("//div[@class='wiki-content']/div[@class='error']/span[@class='error']")
        );
    }

    public void testContentFilterByPersonalLabel()
    {
        final String blogWithPersonalLabel = "blogWithPersonalLabel";
        final String blogWithGlobalLabel = "blogWithGlobalLabel";
        final String personalLabel = "my:label";
        final String globalLabel= "label";

        addLabelToBlogPost(personalLabel, createBlogPost(TEST_SPACE_KEY, blogWithPersonalLabel, StringUtils.EMPTY));
        addLabelToBlogPost(globalLabel, createBlogPost(TEST_SPACE_KEY, blogWithGlobalLabel, StringUtils.EMPTY));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByPersonalLabel",
                "{blog-posts:labels=" + personalLabel + "|content=title}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(blogWithPersonalLabel, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertElementNotPresent(BLOGPOST_XPATH + "[2]");
    }

    public void testContentFilterByGlobalLabel()
    {
        final String blogWithPersonalLabel = "blogWithPersonalLabel";
        final String blogWithGlobalLabel = "blogWithGlobalLabel";
        final String personalLabel = "my:label";
        final String globalLabel= "label";

        addLabelToBlogPost(personalLabel, createBlogPost(TEST_SPACE_KEY, blogWithPersonalLabel, StringUtils.EMPTY));
        addLabelToBlogPost(globalLabel, createBlogPost(TEST_SPACE_KEY, blogWithGlobalLabel, StringUtils.EMPTY));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByGlobalLabel",
                "{blog-posts:labels=" + globalLabel + "|content=title}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(blogWithGlobalLabel, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertElementNotPresent(BLOGPOST_XPATH + "[2]");
    }

    public void testContentFilterByFavouriteLabel()
    {
        final String favoriteBlog = "favoriteBlog";
        final String blogWithGlobalLabel = "blogWithGlobalLabel";
        final String favoriteLabel = "my:favourite"; /* Only works with my:favourite */
        final String globalLabel= "favourite";

        final long favoritePageId = createBlogPost(TEST_SPACE_KEY, favoriteBlog, StringUtils.EMPTY);
        final BlogPostHelper blogPostHelper = getBlogPostHelper(favoritePageId);

        assertTrue(blogPostHelper.read());
        blogPostHelper.markFavourite();
        assertTrue(blogPostHelper.isFavorite());
        
        addLabelToBlogPost(globalLabel, createBlogPost(TEST_SPACE_KEY, blogWithGlobalLabel, StringUtils.EMPTY));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByFavouriteLabel",
                "{blog-posts:labels=" + favoriteLabel + "|content=title}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(favoriteBlog, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertElementNotPresent(BLOGPOST_XPATH + "[2]");
    }

    public void testContentFilterByMultipleOptionalPersonalLabels()
    {
        final String blogWithPersonalLabel1 = "blogWithPersonalLabel1";
        final String blogWithPersonalLabel2 = "blogWithPersonalLabel2";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        addLabelToBlogPost(personalLabel1, createBlogPost(TEST_SPACE_KEY, blogWithPersonalLabel1, StringUtils.EMPTY));
        addLabelToBlogPost(personalLabel2, createBlogPost(TEST_SPACE_KEY, blogWithPersonalLabel2, StringUtils.EMPTY));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByMultipleOptionalPersonalLabels",
                "{blog-posts:labels=" + personalLabel1 + "," + personalLabel2 + "|content=title}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(blogWithPersonalLabel2, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertEquals(blogWithPersonalLabel1, getElementTextByXPath(BLOGPOST_XPATH + "[2]//span[@class='blog-title']/a"));
        assertElementNotPresent(BLOGPOST_XPATH + "[3]");
    }

    public void testContentFilterByMultipleCompulsoryPersonalLabels()
    {
        final String blogWithAllPersonalLabels = "blogWithAllPersonalLabels";
        final String blogWithoutAllPersonalLabels = "blogWithoutAllPersonalLabels";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        final long blogWithAllPersonalLabelId = createBlogPost(TEST_SPACE_KEY, blogWithAllPersonalLabels, StringUtils.EMPTY);

        addLabelToBlogPost(personalLabel1, blogWithAllPersonalLabelId);
        addLabelToBlogPost(personalLabel2, blogWithAllPersonalLabelId);
        addLabelToBlogPost(personalLabel2, createBlogPost(TEST_SPACE_KEY, blogWithoutAllPersonalLabels, StringUtils.EMPTY));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByMultipleCompulsoryPersonalLabels",
                "{blog-posts:labels=+" + personalLabel1 + ",+" + personalLabel2 + "|content=title}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(blogWithAllPersonalLabels, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertElementNotPresent(BLOGPOST_XPATH + "[2]");
    }

    public void testContentFilterByExcludingPersonalLabels()
    {
        final String blogWithoutExcludedLabels = "blogWithoutExcludedLabels";
        final String blogWithExcludedLabels = "blogWithExcludedLabels";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        addLabelToBlogPost(personalLabel1, createBlogPost(TEST_SPACE_KEY, blogWithoutExcludedLabels, StringUtils.EMPTY));

        final long blogWithExcludedLabelsId = createBlogPost(TEST_SPACE_KEY, blogWithExcludedLabels, StringUtils.EMPTY);
        addLabelToBlogPost(personalLabel1, blogWithExcludedLabelsId);
        addLabelToBlogPost(personalLabel2, blogWithExcludedLabelsId);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByExcludingPersonalLabels",
                "{blog-posts:labels=" + personalLabel1 + ",-" + personalLabel2 + "|content=title}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(blogWithoutExcludedLabels, getElementTextByXPath(BLOGPOST_XPATH + "[1]//span[@class='blog-title']/a"));
        assertElementNotPresent(BLOGPOST_XPATH + "[2]");
    }
}
