
jQuery(function($) {
    var FavPages = {
        favPagesEditInProgressArray : new Array(), // use this array to prevent the user from triggering off another labelling operation when one is in progress
        contextPath : undefined,
        i18nMessages : undefined,

        getText : function($favContainer, i18nKey) {
            if (!this.i18nMessages) {
                var i18nMsgs = {};
                $(".favpages-i18n input", $favContainer).each(function() {
                    var key = this.name;
                    var value = i18nMsgs[key];
                    if (value) {
                        if ($.isArray(value)) {
                            value.push(this.value);
                        } else {
                            value = [ value, this.value ];
                        }
                    } else {
                        i18nMsgs[this.name] = this.value;
                    }
                });

                this.i18nMessages = i18nMsgs;
            }

            return this.i18nMessages[i18nKey];
        },

        getContextPath : function() {
            if (!this.contextPath)
                this.contextPath = $("#confluence-context-path").attr("content");
            return this.contextPath;
        },

        toggleStar : function($favContainer, linkElement, iconElement) {
            iconElement.toggleClass("icon-add-fav");
            iconElement.toggleClass("icon-remove-fav");
            if (iconElement.hasClass("icon-add-fav"))
                linkElement["title"] = this.getText($favContainer, "favourite.add.page");
            else
                linkElement["title"] = this.getText($favContainer, "favourite.remove.page");
        },

        init : function($favContainer) {
            $("a.favorite-toggle", $favContainer).each(function() {
                $(this).click(function() {
                    var toggleId = this.id,
                        toggleLink = $(this),
                        toggleIcon = toggleLink.find("span.icon");
                    if (FavPages.favPagesEditInProgressArray[toggleId] == null) {
                        FavPages.favPagesEditInProgressArray[toggleId] = true;

                        AJS.safe.ajax({
                            cache : false,
                            url: FavPages.getContextPath() + (toggleIcon.hasClass("icon-remove-fav") ? "/labels/removefavourite.action" : "/labels/addfavourite.action"),
                            type: "POST",
                            data: {"entityId": this.id.substring("entityStar-".length) },
                            success: function() {
                                FavPages.toggleStar($favContainer, toggleLink, toggleIcon);
                                FavPages.favPagesEditInProgressArray[toggleId] = null;
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert("An error occurred toggling favourite. Status: " + textStatus + " ThrownError:" + errorThrown);
                            }
                        });
                    }
                    return false;
                });
            });
        }
    };

    $("div.favpages-container").each(function() {
        FavPages.init($(this));
    });
});
