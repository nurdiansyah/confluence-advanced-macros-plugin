package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.search.v2.SearchResult;

/**
 * Responsible for creating an update item out of a the information contained in a search result.
 */
public interface UpdateItemFactory
{
    /**
     * Constructs an update item from a search result or returns null if the search result is stale.
     * Stale search results occur when the corresponding database object has been removed and the index has not caught up yet.
     *
     * @param searchResult the search result
     * @return an update item or null if the search result is stale.
     */
    UpdateItem get(SearchResult searchResult);
}
