package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.search.v2.ISearch;

/**
 * Builds recent changes search. Used primarily to group together the logic that composes a recent change search off v2
 * search objects so that it can be re-used (say in a macros and in actions).
 */
public interface RecentChangesSearchBuilder
{
    /**
     * Returns a changes search.
     * @param queryParams
     * @param paginationParams
     * @return a changes search.
     * @throws IllegalArgumentException if any of the parameter values in params are invalid. 
     */
    ISearch getChangesSearch(QueryParameters queryParams, PaginationParameters paginationParams);
}
