package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.confluence.util.actions.AlphabeticalLabelGroupingSupport;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class ListLabelsMacro extends BaseMacro implements Macro
{
    private static final String TOKEN_ALL = "@all";

    public static final String PARAM_SPACEKEY = "spaceKey";
    public static final String PARAM_EXCLUDEDLABELS = "excludedLabels";

    private LabelManager labelManager;
    private SpaceManager spaceManager;

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    @Override
    public String execute(final Map parameters, final String body, final ConversionContext conversionContext)
    {
        try
        {
            return execute(parameters, body, conversionContext.getPageContext());
        }
        catch (MacroException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        String spaceKey = (String) parameters.get(PARAM_SPACEKEY);
        String excludedLabelsParam = (String) parameters.get(PARAM_EXCLUDEDLABELS);
        String[] excludedLabels = null;
        if (!StringUtils.isBlank(excludedLabelsParam))
        {
            excludedLabels = excludedLabelsParam.trim().split("\\s*,\\s*");
            Arrays.sort(excludedLabels);
        }

        // if no space key provided, use the current space
        if (StringUtils.isBlank(spaceKey))
        {
            PageContext pCtx = (PageContext)renderContext;
            spaceKey = pCtx.getSpaceKey();
        }

        final AlphabeticalLabelGroupingSupport alphaSupport = getAlphaSupport(spaceKey, excludedLabels);

        Map<String, Object> contextMap = getDefaultVelocityContext();
        contextMap.put("alphaSupport", alphaSupport);
        contextMap.put("space", spaceManager.getSpace(spaceKey));
        contextMap.put("spaceKey",spaceKey);
        return getRenderedTemplate(contextMap);
    }

    public AlphabeticalLabelGroupingSupport getAlphaSupport(@Nonnull final String spaceKey, @Nullable final String[] sortedExcludedLabels)
    {
        Collection<Object> labels;

        if (TOKEN_ALL.equals(spaceKey))
        {
            Set<Object> labelsSet = new TreeSet<Object>();
            List<Space> spaces = spaceManager.getAllSpaces();
            for (Space space : spaces)
            {
                labelsSet.addAll(labelManager.getLabelsInSpace(space.getKey()));
            }
            labels = Arrays.asList(labelsSet.toArray());
        }
        else
        {
            labels = labelManager.getLabelsInSpace(spaceKey);
        }

        if (!ArrayUtils.isEmpty(sortedExcludedLabels))
        {
            labels = Collections2.filter(labels, new Predicate<Object>()
            {
                @Override
                public boolean apply(@Nullable final Object input)
                {
                    return input != null && Arrays.binarySearch(sortedExcludedLabels, input.toString()) < 0;
                }
            });
        }

        return new AlphabeticalLabelGroupingSupport(labels);
    }

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }
    
    ///CLOVER:OFF
    protected String getRenderedTemplate(Map<String, Object> contextMap) {
        return VelocityUtils.getRenderedTemplate("/com/atlassian/confluence/plugins/macros/advanced/listlabelsmacro.vm", contextMap);
    }

    protected Map<String, Object> getDefaultVelocityContext() {
        return MacroUtils.defaultVelocityContext();
    }
    ///CLOVER:OFF
}
