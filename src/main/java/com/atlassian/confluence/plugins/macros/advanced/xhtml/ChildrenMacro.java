package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.LinkRenderer;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.PageContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ContentComparatorFactory;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A minimal impact re-implementation of the v2 PageChildrenMacro which better understands ConversionContext with the
 * purpose of rendering links correctly on export (CONFDEV-1285).
 * <p>
 * This is really only a starting point for a full on conversion to an XHTML version of this macro. 
 */
public class ChildrenMacro extends ContentFilteringMacro implements Macro
{
    private PageManager pageManager;
    private SpaceManager spaceManager;
    private LinkRenderer viewLinkRenderer;
    private PermissionManager permissionManager;
    private ContentPermissionManager contentPermissionManager;
    private ExcerptHelper excerptHelper;
    private ConfluenceActionSupport confluenceActionSupport; /* For i18n */
    private WebResourceManager webResourceManager;

    private Comparator<Page> sort;

    private static boolean pdlEnabled = Long.parseLong(GeneralUtil.getBuildNumber()) >= 4000;

    public boolean isInline()
    {
        return getOutputType() == OutputType.INLINE;
    }

    public boolean hasBody()
    {
        return getBodyType() != Macro.BodyType.NONE;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }
    
    /**
     * This XHTML execute method is where all the work is actually done. It's called by the {@link #execute(MacroExecutionContext)} method.
     */
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
    {
        webResourceManager.requireResource("confluence.macros.advanced:children-resource");

        String pageTitle = parameters.get("page");
        PageContext pageContext = context != null && context.getPageContext() != null ? context.getPageContext() : new PageContext();

        List<Page> children;

        try
        {
            if (pageTitle != null && (pageTitle.equals("/") || pageTitle.endsWith(":")))
                 children = getRootPagesForSpace(pageContext, pageTitle);
            else
                 children = getChildrenFromPage(pageContext, pageTitle);
        }
        catch (IllegalArgumentException e)
        {
            return RenderUtils.blockError(getConfluenceActionSupport().getText("children.error.unable-to-render"), e.getMessage());
        }

        boolean excerpt = Boolean.valueOf(parameters.get("excerpt"));

        int depth = 1;

        if ("true".equalsIgnoreCase(parameters.get("all")) || "all".equalsIgnoreCase(parameters.get("depth")))
        {
            depth = 0;
        }

        if (parameters.get("depth") != null && !"all".equalsIgnoreCase(parameters.get("depth")))
        {
            try
            {
                depth = Integer.parseInt(parameters.get("depth"));
            }
            catch (NumberFormatException e)
            {
                return RenderUtils.blockError(
                        getConfluenceActionSupport().getText("children.error.unable-to-render"),
                        getConfluenceActionSupport().getText(
                                "children.error.invalid-depth",
                                new String[] { StringEscapeUtils.escapeHtml(parameters.get("depth"))})
                );
            }
        }

        // configure the sorting alg.
        sort = configureComparator(parameters);

        // limit the number of results at the root level. but first, we need to sort to ensure
        // the correct results are displayed.
        if (sort != null)
        {
            Collections.sort(children, sort);
        }

        if (TextUtils.stringSet(parameters.get("first")))
        {
            try
            {
                int first = Integer.parseInt(parameters.get("first"));
                if (first > 0 && (first < children.size()))
                {
                    children = children.subList(0, first);
                }
            }
            catch (NumberFormatException e)
            {
                // noop
            }
        }


        if (TextUtils.stringSet(parameters.get("style")))
        {
            String style = parameters.get("style");
            if (style.matches("h[1-6]"))
                return printChildrenUnderHeadings(children, depth, excerpt, context, style.charAt(1));
            else
                return RenderUtils.blockError(
                        getConfluenceActionSupport().getText("children.error.unable-to-render"),
                        getConfluenceActionSupport().getText(
                                "children.error.unknown-style",
                                new String[] { StringEscapeUtils.escapeHtml(style) })
                );

        }

        return printChildren(children, depth, excerpt, context);
    }
    
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public String execute(MacroExecutionContext ctx) throws MacroException
    {
        try
        {
            return execute(ctx.getParams(), ctx.getBody(), new DefaultConversionContext(ctx.getPageContext()));
        }
        catch (MacroExecutionException ex)
        {
            throw new MacroException(ex.getCause() != null ? ex.getCause() : ex);
        }
    }
    
    private Comparator<Page> configureComparator(Map<String, String> parameters)
    {
        String sortType = (String) parameters.get("sort");
        boolean reverse = Boolean.valueOf((String) parameters.get("reverse"));
        return ContentComparatorFactory.getComparator(sortType, reverse);
    }

    private List<Page> getRootPagesForSpace(PageContext pageContext, String pageTitle)
    {
        Space space = null;

        if ("/".equals(pageTitle))
            space = getCurrentSpace(pageContext);
        else if (pageTitle.length() > 1 && pageTitle.endsWith(":"))
            space = spaceManager.getSpace(pageTitle.substring(0, pageTitle.length() - 1));

        if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, space))
            throw new IllegalArgumentException(
                    getConfluenceActionSupport().getText(
                            "children.error.space-does-not-exists",
                            new String[] { StringEscapeUtils.escapeHtml(pageTitle) })
            );

        List<Page> pages = permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, pageManager.getTopLevelPages(space));

        if (pageContext.getEntity() != null)
            removeThisPageFromList(pages, pageContext.getEntity());

        return pages;
    }

    private Space getCurrentSpace(PageContext pageContext)
    {
        Space space;
        ContentEntityObject obj = pageContext.getEntity();
        if (obj instanceof PageContentEntityObject)
            obj = ((PageContentEntityObject)obj).getPage();

        if (obj instanceof SpaceContentEntityObject)
            space = ((SpaceContentEntityObject)obj).getSpace();
        else
            throw new IllegalArgumentException(getConfluenceActionSupport().getText("children.error.content-not-belong-to-space"));
        return space;
    }

    private void removeThisPageFromList(List<Page> pages, ContentEntityObject thisPage)
    {
        for (Iterator<Page> it = pages.iterator(); it.hasNext();)
        {
            Page page = (Page) it.next();
            if (page.getId() == thisPage.getId())
                it.remove();
        }
    }

    private List<Page> getChildrenFromPage(PageContext pageContext, String pageTitle)
    {
        List<Page> children;
        ContentEntityObject target = getPage(pageContext, pageTitle);

        if (target == null)
        {
            if (TextUtils.stringSet(pageTitle))
                throw new IllegalArgumentException(getConfluenceActionSupport().getText(
                        "children.error.page-not-found",
                        new String[] { StringEscapeUtils.escapeHtml(pageTitle) }));
            else
                throw new IllegalArgumentException(getConfluenceActionSupport().getText("children.error.macro-works-on-only-pages"));
        }

        if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, target))
            throw new IllegalArgumentException(getConfluenceActionSupport().getText(
                    "children.error.page-not-found",
                    new String[] { StringEscapeUtils.escapeHtml(pageTitle) }));

        if (!(target instanceof Page))
            throw new IllegalArgumentException(
                    getConfluenceActionSupport().getText(
                            "children.error.can-only-find-children-of-a-page",
                            new String[] { target.getType() })
            );

        return contentPermissionManager.getPermittedChildren((Page) target, AuthenticatedUserThreadLocal.getUser());
    }


    private ContentEntityObject getPage(PageContext context, String pageTitleToRetrieve)
    {
        if (!TextUtils.stringSet(pageTitleToRetrieve))
            return getCurrentPage(context);

        String spaceKey = context.getSpaceKey();
        String pageTitle = pageTitleToRetrieve;

        int colonIndex = pageTitleToRetrieve.indexOf(":");
        if (colonIndex != -1 && colonIndex != pageTitleToRetrieve.length() - 1)
        {
            spaceKey = pageTitleToRetrieve.substring(0, colonIndex);
            pageTitle = pageTitleToRetrieve.substring(colonIndex + 1);
        }

        return pageManager.getPage(spaceKey, pageTitle);
    }

    private ContentEntityObject getCurrentPage(PageContext context)
    {
        ContentEntityObject entity = ContentIncludeStack.peek();
        if (entity instanceof Page)
            return entity;

        return context.getEntity();
    }

    private String printChildren(List<Page> pages, int depth, boolean excerpt, ConversionContext conversionContext) throws MacroExecutionException
    {
        StringBuffer buf = new StringBuffer();
        printChildren(pages, buf, depth, excerpt, conversionContext);
        return buf.toString();
    }

    private String printChildrenUnderHeadings(List<Page> children, int depth, boolean excerpt, ConversionContext conversionContext, char headerType) throws MacroExecutionException
    {

        StringBuffer buffer = new StringBuffer();

        for (Page child : children)
        {
            buffer.append("<h").append(headerType).append(">");
            buffer.append(makePageLink(child, conversionContext));
            buffer.append("</h").append(headerType).append(">\n");

            if (excerpt)
            {
                String renderedExcerpt = excerptHelper.getExcerptSummary(child);
                if (TextUtils.stringSet(renderedExcerpt))
                    buffer.append("<p>").append(renderedExcerpt).append("</p>\n");
            }

            if (depth != 1)
            {
                printChildren(contentPermissionManager.getPermittedChildren(child, AuthenticatedUserThreadLocal.getUser()), buffer, depth - 1, excerpt, conversionContext);
            }
        }

        return buffer.toString();
    }

    private void printChildren(List<Page> children, StringBuffer buffer, int depth, boolean excerpt, ConversionContext conversionContext) throws MacroExecutionException
    {
        if (children.size() > 0)
        {
            if (sort != null)
            {
                Collections.sort(children, sort);
            }

            buffer.append("<ul class='childpages-macro'>");
            for (Page child: children)
            {
                buffer.append("<li>").append(makePageLink(child, conversionContext));

                if (excerpt)
                {
                    String renderedExcerpt = excerptHelper.getExcerptSummary(child);
                    if (TextUtils.stringSet(renderedExcerpt))
                        buffer.append(" &mdash; <span class=\"smalltext\">").append(renderedExcerpt).append("</span>");
                }

                if (depth != 1)
                {
                    printChildren(contentPermissionManager.getPermittedChildren(child, AuthenticatedUserThreadLocal.getUser()), buffer, depth - 1, excerpt, conversionContext);
                }
                buffer.append("</li>");
            }
            buffer.append("</ul>");
        }
    }

    private String makePageLink(Page child, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            return viewLinkRenderer.render(child, conversionContext);
        }
        catch (XhtmlException e)
        {
            throw new MacroExecutionException(e);
        }
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setContentPermissionManager(ContentPermissionManager contentPermissionManager)
    {
        this.contentPermissionManager = contentPermissionManager;
    }
    
    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
        this.excerptHelper = excerptHelper;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    protected ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }

    public void setViewLinkRenderer(LinkRenderer viewLinkRenderer)
    {
        this.viewLinkRenderer = viewLinkRenderer;
    }    
}
