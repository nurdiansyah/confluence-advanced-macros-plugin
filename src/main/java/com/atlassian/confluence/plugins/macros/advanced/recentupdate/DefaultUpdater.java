package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.util.i18n.I18NBean;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class DefaultUpdater implements Updater
{
    private final String username;
    private final I18NBean i18n;

    private UserProfileLink userProfileLink;
    private UserLink userLink;

    public DefaultUpdater(String username, I18NBean i18n)
    {
        this.username = username;
        this.i18n = i18n;
    }

    public String getUsername()
    {
        return username;
    }

    public String getLinkedProfilePicture()
    {
        if (userProfileLink == null)
            userProfileLink = new UserProfileLink(username, i18n);

        return userProfileLink.toString();
    }

    public String getLinkedFullName()
    {
        if (userLink == null)
            userLink = new UserLink(username, i18n);

        return userLink.toString();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(username).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this) return true;
        if (!(obj instanceof DefaultUpdater)) return false;

        DefaultUpdater that = (DefaultUpdater) obj;
        return new EqualsBuilder().append(username, that.username).isEquals();
    }
}
