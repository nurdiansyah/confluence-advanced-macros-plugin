package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.links.UnpermittedLink;
import com.atlassian.renderer.links.UnresolvedLink;
import org.apache.commons.lang.StringUtils;

public class DefaultPageProvider implements PageProvider
{
    private LinkResolver linkResolver;
    private PermissionManager permissionManager;
    private I18NBeanFactory i18NBeanFactory;

    public ContentEntityObject resolve(String location, RenderContext context) throws NotAuthorizedException
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();

        if (StringUtils.isEmpty(location))
        {
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.no.location"));
        }

        Link link = linkResolver.createLink(context, location);

        if (link == null)
        {
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.no.link", new String[]{location}));
        }
        else if (link instanceof UnpermittedLink)
        {
            throw new NotAuthorizedException(i18NBean.getText(
                    "confluence.macros.advanced.include.error.user.not.authorized",
                    new String[]{AuthenticatedUserThreadLocal.getUsername(), location}
            ));
        }
        else if (link instanceof UnresolvedLink)
        {
            return null;
        }
        else if (!(link instanceof AbstractPageLink))
        {
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.invalid.content-entity"));
        }

        // NOTE: UnpermittedLink will NOT check the user's permissions to view pages.
        if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, ((AbstractPageLink) link).getDestinationContent()))
        {
            throw new NotAuthorizedException(i18NBean.getText(
                    "confluence.macros.advanced.include.error.user.not.authorized",
                    new String[]{AuthenticatedUserThreadLocal.getUsername(), location}
            ));
        }

        return ((AbstractPageLink) link).getDestinationContent();
    }

    public void setLinkResolver(LinkResolver linkResolver)
    {
        this.linkResolver = linkResolver;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setUserI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }
}
