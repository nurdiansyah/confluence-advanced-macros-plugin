package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.Page;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartHandlePaginationParameters;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;

import java.util.List;

public class DefaultRecentUpdatesManager implements RecentUpdatesManager
{
    private final SearchManager searchManager;
    private final RecentChangesSearchBuilder searchBuilder;

    public DefaultRecentUpdatesManager(SearchManager searchManager)
    {
        this.searchManager = searchManager;
        this.searchBuilder = new DefaultRecentChangesSearchBuilder();
    }

    public DefaultRecentUpdatesManager(SearchManager searchManager, RecentChangesSearchBuilder searchBuilder)
    {
        this.searchManager = searchManager;
        this.searchBuilder = searchBuilder;
    }

    public Page<SearchResult> find(QueryParameters queryParams, PaginationParameters paginationParams)
    {
        ISearch changesSearch = searchBuilder.getChangesSearch(queryParams, paginationParams);

        List<SearchResult> searchResults;
        try
        {
            searchResults = searchManager.search(changesSearch).getAll();
        }
        catch (InvalidSearchException e)
        {
            return Page.emptyPage();
        }

        final Page<SearchResult> page = new Page<SearchResult>(searchResults, paginationParams.getPageSize());

        if (page.morePages())
            page.setNextPageParameters(new StartHandlePaginationParameters(searchResults.get(searchResults.size() - 1).getHandle(), paginationParams.getPageSize()));

        return page;
    }
}
