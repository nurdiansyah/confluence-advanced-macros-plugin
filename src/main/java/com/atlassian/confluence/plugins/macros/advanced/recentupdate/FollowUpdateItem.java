package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;

public class FollowUpdateItem extends AbstractUpdateItem
{
    private final PersonalInformation followeePersonalInfo;
    private final String follower;

    /**
     * @param searchResult
     * @param dateFormatter
     * @param followeePersonalInfo personal info of the person being followed
     * @param follower person doing the following
     */
    public FollowUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, PersonalInformation followeePersonalInfo, String follower, I18NBean i18n)
    {
        super(searchResult, dateFormatter, i18n, "content-type-follow");
        this.followeePersonalInfo = followeePersonalInfo;
        this.follower = follower;
    }

    @Override
    public Updater getUpdater()
    {
        return new DefaultUpdater(follower, i18n);
    }

    public String getLinkedUpdateTarget()
    {
        return String.format("<a href=\"%s%s\">%s</a>", RequestCacheThreadLocal.getContextPath(), followeePersonalInfo.getUrlPath(), followeePersonalInfo.getDisplayTitle());
    }

    public String getDescriptionAndDateKey()
    {
        return "update.item.desc.follow";
    }

    public String getDescriptionAndAuthorKey()
    {
        return "update.item.desc.author.follow";
    }

}
