package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;

/**
 * This action serves changes to the "get more" requests on the "sidebar" theme.
 */
public class SideBarThemeChangesAction extends AbstractChangesAction
{
    protected Theme getTheme()
    {
        return Theme.sidebar;
    }
}
