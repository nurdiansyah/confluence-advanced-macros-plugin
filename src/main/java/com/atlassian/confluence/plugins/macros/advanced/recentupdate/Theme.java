package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

/**
 * Recent update theme
 */
public enum Theme
{
    /**
     * Displays profile photos
     */
    social,
    /**
     * Compact view with most of what you would expect
     */
    concise,
    /**
     * Does not display author
     */
    sidebar
}
