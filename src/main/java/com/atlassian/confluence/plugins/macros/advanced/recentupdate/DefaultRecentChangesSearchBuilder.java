package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.macro.query.params.AuthorParameter;
import com.atlassian.confluence.macro.query.params.ContentTypeParameter;
import com.atlassian.confluence.macro.query.params.LabelParameter;
import com.atlassian.confluence.macro.query.params.SpaceKeyParameter;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.*;
import com.atlassian.confluence.search.v2.query.AllQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.searchfilter.LastModifierSearchFilter;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.ModifiedSort;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * See super javadoc.
 */
public class DefaultRecentChangesSearchBuilder implements RecentChangesSearchBuilder
{
    public ISearch getChangesSearch(QueryParameters queryParams, PaginationParameters paginationParams)
    {
        BooleanQueryFactory booleanQueryFactory;
        SearchFilter searchFilter = SiteSearchPermissionsSearchFilter.getInstance();
        ResultFilter resultFilter = null;

        try
        {
            booleanQueryFactory = new BooleanQueryFactory();

            if (StringUtils.isNotBlank(queryParams.getLabels()))
                booleanQueryFactory.addMust(getLabelQuery(queryParams.getLabels()));

            booleanQueryFactory.addMust(getContentTypeQuery(queryParams.getContentTypes()));

            if (StringUtils.isNotBlank(queryParams.getAuthors()))
            {
                SearchFilter authorFilter = getAuthorQuery(queryParams.getAuthors());
                if (authorFilter != null)
                    searchFilter = searchFilter.and(authorFilter);
            }

            if (StringUtils.isNotBlank(queryParams.getSpaceKeys()))
                booleanQueryFactory.addMust(getSpaceQuery(queryParams.getSpaceKeys()));

            if (paginationParams instanceof ResultFilterFactory)
                resultFilter = ((ResultFilterFactory) paginationParams).getResultFilter();
        }
        catch (ParameterException e) // thrown by any one of the getXXXXQuery() methods if they fail to build the query due to a badly-formed/invalid parameter value
        {
            throw new IllegalArgumentException(e);
        }

        return new ChangesSearch(booleanQueryFactory.toBooleanQuery(), ModifiedSort.DEFAULT, searchFilter, resultFilter);
    }

    private SearchQuery getSpaceQuery(String spaceKeys) throws ParameterException
    {
        return new SpaceKeyParameter().findValue(getMacroExecutionContext(Collections.singletonMap("spaces", spaceKeys))).toBooleanQuery();
    }

    private SearchQuery getContentTypeQuery(String contentType) throws ParameterException
    {
        if (StringUtils.isBlank(contentType))
        {
            BooleanQueryFactory booleanQueryFactory = new BooleanQueryFactory();
            booleanQueryFactory.addMust(AllQuery.getInstance());
            booleanQueryFactory.addMustNot(new ContentTypeQuery(ContentTypeEnum.MAIL));
            return booleanQueryFactory.toBooleanQuery();
        }

        return new ContentTypeParameter().findValue(getMacroExecutionContext(Collections.singletonMap("type", contentType))).toBooleanQuery();
    }

    private SearchQuery getLabelQuery(String labels) throws ParameterException
    {
        return new LabelParameter().findValue(getMacroExecutionContext(Collections.singletonMap("labels", labels))).toBooleanQuery();
    }

    private SearchFilter getAuthorQuery(String authorsParamValue) throws ParameterException
    {
        final Set<String> authors = new AuthorParameter().findValue(getMacroExecutionContext(Collections.singletonMap("author", authorsParamValue)));
        if (!authors.isEmpty())
            return new LastModifierSearchFilter(authors.toArray(new String[authors.size()]));
        else
            return null;
    }

    /**
     * Remove this when we decouple {@link com.atlassian.confluence.macro.params.Parameter} from {@link com.atlassian.confluence.macro.MacroExecutionContext}.
     */
    private MacroExecutionContext getMacroExecutionContext(Map<String, String> params)
    {
        return new MacroExecutionContext(params, null, new PageContext());
    }
}
