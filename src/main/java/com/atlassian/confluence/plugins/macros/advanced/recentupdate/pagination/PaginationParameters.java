package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

/**
 * Type that groups together pagination parameters such a page size and offset.
 * @param <T> the generic type of the offset
 */
public interface PaginationParameters<T>
{
    int DEFAULT_SIZE = 15;

    /**
     * Maximum page size allowed. Two reasons:
     * <ul>
     * <li>Prevent abuse</li>
     * <li>Retrieving any page between 100th and 200th results inclusive will incur the expense of 2 searches given our use of {@link org.apache.lucene.search.Hits}.
     * We don't want to incur the cost of more than 2 searches so restrict to less than 200.</li>
     * </ul>
     */
    int MAX_PAGE_SIZE = 200;

    int getPageSize();

    /**
     * @return the offset
     */
    T getOffset();

    /**
     * @return name of the offset (say startIndex or startHandle)
     */
    String getOffSetName(); 
}
