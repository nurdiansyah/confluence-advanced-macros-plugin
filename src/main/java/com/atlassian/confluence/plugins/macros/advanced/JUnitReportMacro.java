package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.renderer.radeox.ContentPreserver;
import com.atlassian.confluence.renderer.radeox.macros.AbstractHtmlGeneratingMacro;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.renderer.radeox.macros.junit.report.TestReport;
import com.atlassian.confluence.renderer.radeox.macros.junit.report.TestSuite;
import com.atlassian.confluence.renderer.radeox.macros.junit.JUnitTestCaseReport;
import com.atlassian.confluence.renderer.radeox.macros.junit.JUnitTestCaseFailureReport;
import com.atlassian.confluence.util.io.IOUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.core.exception.InfrastructureException;
import com.opensymphony.util.TextUtils;
import org.apache.commons.digester.Digester;
import org.apache.log4j.Category;
import org.radeox.macro.parameter.MacroParameter;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

/**
 * A macro to fetch JUnit result XML files and display/import them as HTML
 */
public class JUnitReportMacro extends AbstractHtmlGeneratingMacro
{
    public static final Category log = Category.getInstance(JUnitReportMacro.class);
    public static final EntityResolver NULL_ENTITY_RESOLVER = new EntityResolver()
    {
        @Override
        public InputSource resolveEntity(String s, String s1) throws SAXException, IOException
        {
            return new InputSource(new ByteArrayInputStream(new byte[0]));
        }
    };
    private String[] myParamDescription = new String[]{"?1: url", "?2: directory", "?3: reportdetail", "?4: debug"};
    private ContentPreserver contentPreserver = new ContentPreserver();

    public JUnitReportMacro()
    {
        addEmoticonsAsSpecial();
    }

	protected void addEmoticonsAsSpecial() {
		contentPreserver.addEmoticonsAsSpecial();
	}

    public String getName()
    {
        return "junitreport";
    }

    public String[] getParamDescription()
    {
        return myParamDescription;
    }

    protected String getHtml(MacroParameter macroParameter) throws IllegalArgumentException, IOException
    {
        String url = TextUtils.noNull(macroParameter.get("url")).trim();
        String directory = TextUtils.noNull(macroParameter.get("directory")).trim();
        String reportdetail = macroParameter.get("reportdetail");
        String debugString = macroParameter.get("debug");
        Boolean debug = Boolean.FALSE;
        if(reportdetail==null) {
            reportdetail = TestSuite.REPORT_DETAIL_PER_FIXTURE;
        }
        if(debugString!=null) {
            debug = Boolean.valueOf(debugString);
        }
        TestReport report;

        try
        {
            if (!TextUtils.stringSet(directory))
                report = prepareJUnitTestReport(url);
            else
                report = prepareJUnitDirectoryReport(directory);

            Map<String, Object> contextMap = getDefaultVelocityContext();
            contextMap.put("url", url);
            contextMap.put("report", report);
            contextMap.put("reportDetail", reportdetail);
            contextMap.put("debug", debug);
            String content = generateRenderedTemplate(contextMap);

            return doContentPreserver(content);
        }
        catch (Exception e)
        {
            log.error("Error while trying to assemble the JUnit Report result!", e);
            throw new IOException(e.getMessage());
        }
    }

	protected String doContentPreserver(String content) {
		return contentPreserver.doPreserve(content);
	}

    private TestReport prepareJUnitDirectoryReport(String dirName) throws MalformedURLException, URISyntaxException
    {
        File directory = createNewFile(dirName);
        File[] fileList = directory.listFiles(new FileFilter() {
            public boolean accept(File file)
            {
                return file.isFile() && file.getName().endsWith(".xml");
            }
        });

        JUnitTestCaseReport report = newJUnitTestCaseReport();
        report.setName("JUnit Results");
        if(fileList != null) {
            for (int i = 0; i < fileList.length; i++)
            {
                File file = fileList[i];
                report.addTest(prepareJUnitTestReport(file.toURL().toExternalForm()));
            }
        }

        return report;
    }

	protected File createNewFile(String dirName) throws URISyntaxException {
		return new File(new URI(dirName));
	}

    private TestReport prepareJUnitTestReport(String url)
    {
        JUnitTestCaseReport report = newJUnitTestCaseReport();
        Digester digester = new Digester(createXmlReader());
        digester.setEntityResolver(NULL_ENTITY_RESOLVER);

        digester.push(report);
        digester.addSetProperties("testsuite", "name", "name");
        //digester.addSetProperties("testsuite", "tests", "testsCount");
        //digester.addSetProperties("testsuite", "failures", "failuresCount");
        //digester.addSetProperties("testsuite", "errors", "errorsCount");
        digester.addSetProperties("testsuite", "time", "timeAsString");

//        digester.addBeanPropertySetter("testsuite/system-out", "systemOutput");
//        digester.addBeanPropertySetter("testsuite/system-err", "systemError");
//        digester.addCallMethod("testsuite/properties/property", "putProperty", 2);
//        digester.addCallParam("testsuite/properties/property", 0, "name");
//        digester.addCallParam("testsuite/properties/property", 1, "value");

        digester.addObjectCreate("testsuite/testcase", JUnitTestCaseReport.class);
        digester.addSetProperties("testsuite/testcase", "name", "name");
        digester.addSetProperties("testsuite/testcase", "time", "timeAsString");

        digester.addObjectCreate("testsuite/testcase/failure", JUnitTestCaseFailureReport.class);
        digester.addSetProperties("testsuite/testcase/failure");
        digester.addBeanPropertySetter("testsuite/testcase/failure", "content");
        digester.addSetNext("testsuite/testcase/failure", "setFailure");

        digester.addObjectCreate("testsuite/testcase/error", JUnitTestCaseFailureReport.class);
        digester.addSetProperties("testsuite/testcase/error", "message", "message");
        digester.addSetProperties("testsuite/testcase/error", "type", "type");
        digester.addBeanPropertySetter("testsuite/testcase/error", "content");
        digester.addSetNext("testsuite/testcase/error", "setFailure");

        digester.addSetNext("testsuite/testcase", "addTest");
        InputStream is = null;
        try
        {
            is = openUrlStream(url);
            parseInputStream(digester, is);
        }
        catch (Exception e)
        {
            log.error("Error parsing JUnit report file!", e);
            throw new InfrastructureException("Error parsing JUnit report file: " + e.toString(), e);
        }
        finally
        {
            IOUtils.close(is);
        }
        return report;
    }

    private XMLReader createXmlReader()
    {
        try{
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setValidating(false);
            spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            XMLReader xr = spf.newSAXParser().getXMLReader();
            xr.setEntityResolver(NULL_ENTITY_RESOLVER);
            return xr;
        }
        catch (ParserConfigurationException e)
        {
            throw new RuntimeException("Unable to create XML parser");
        }
        catch (SAXException e)
        {
            throw new RuntimeException("Unable to create XML parser");
        }
    }

    protected JUnitTestCaseReport newJUnitTestCaseReport() {
		return new JUnitTestCaseReport();
	}

	protected Object parseInputStream(Digester digester, InputStream is)
			throws IOException, SAXException {
		return digester.parse(is);
	}

	protected InputStream openUrlStream(String url) throws IOException,
			MalformedURLException {
		return new URL(url).openStream();
	}
	
    ///CLOVER:OFF
    protected String generateRenderedTemplate(Map<String, Object> contextMap) {
        return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/plugins/macros/advanced/templates/junitreport.vm", contextMap);
    }

    protected Map<String, Object> getDefaultVelocityContext() {
        return MacroUtils.defaultVelocityContext();
    }
    ///CLOVER:OFF
}
