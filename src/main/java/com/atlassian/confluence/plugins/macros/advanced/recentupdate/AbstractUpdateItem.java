package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.datetime.FriendlyDateFormatter;
import com.atlassian.confluence.core.datetime.RequestTimeThreadLocal;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.Message;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import org.apache.commons.lang.StringUtils;

import static java.util.Arrays.asList;

public abstract class AbstractUpdateItem implements UpdateItem
{
    final SearchResult searchResult;
    final FriendlyDateFormatter dateFormatter;
    private final String iconClass;
    private DefaultUpdater updater;
    final I18NBean i18n;

    public AbstractUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, I18NBean i18n, String iconClass)
    {
        this.searchResult = searchResult;
        this.dateFormatter = new FriendlyDateFormatter(RequestTimeThreadLocal.getTimeOrNow(), dateFormatter);
        this.iconClass = iconClass;
        this.i18n = i18n;
    }

    @HtmlSafe
    public String getDescriptionAndDate()
    {
        return i18n.getText(getDescriptionAndDateKey(), asList(getFormattedDate()));
    }

    /**
     * @return an i18n key for the description of this update (that includes the date).
     */
    protected abstract String getDescriptionAndDateKey();

    @HtmlSafe
    public String getDescriptionAndAuthor()
    {
        return i18n.getText(getDescriptionAndAuthorKey(), asList(getUpdater().getLinkedFullName()));
    }

    /**
     * @return an i18n key for the description of this update (that includes the author).
     */
    protected abstract String getDescriptionAndAuthorKey();

    /**
     * @return the username of the updater
     */
    public Updater getUpdater()
    {
        if (updater == null)
            updater = new DefaultUpdater(searchResult.getLastModifier(), i18n);

        return updater;
    }

    @HtmlSafe
    public String getUpdateTargetTitle()
    {
        return GeneralUtil.htmlEncode(searchResult.getDisplayTitle());
    }

    @HtmlSafe
    public String getLinkedUpdateTargetForHtmlExport()
    {
        return getLinkedUpdateTarget();
    }
    
    @HtmlSafe
    public String getLinkedUpdateTarget()
    {
        return String.format("<a href=\"%s%s\" title=\"%s\">%s</a>", getRequestCacheThreadLocalContextPath(), getUpdateTargetUrl(), getUpdateTargetToolTip(), getUpdateTargetTitle());
    }

    protected String getRequestCacheThreadLocalContextPath()
    {
        return RequestCacheThreadLocal.getContextPath();
    }

    @HtmlSafe
    protected String getUpdateTargetToolTip()
    {
        return "";
    }

    @HtmlSafe
    public String getLinkedSpace()
    {
        if (StringUtils.isBlank(searchResult.getSpaceKey()))
            return null;

        return String.format("<a href=\"%s%s\">%s</a>", getRequestCacheThreadLocalContextPath(), "/display/" + GeneralUtil.urlEncode(searchResult.getSpaceKey()), GeneralUtil.htmlEncode(searchResult.getSpaceName()));
    }

    @HtmlSafe
    public String getSpaceName()
    {
        if (StringUtils.isBlank(searchResult.getSpaceName()))
            return null;

        return GeneralUtil.htmlEncode(searchResult.getSpaceName());
    }

    protected String getUpdateTargetUrl()
    {
        return searchResult.getUrlPath();
    }

    public String getIconClass()
    {
        return iconClass;
    }

    /**
     * @return a date formatted according to the {@link com.atlassian.confluence.core.DateFormatter} used to construct this update item.
     */
    public String getFormattedDate()
    {
        final Message message = dateFormatter.getFormatMessage(searchResult.getLastModificationDate());
        return i18n.getText(message.getKey(), message.getArguments());
    }

    public String getBody() 
    {
        return null;
    }
}
