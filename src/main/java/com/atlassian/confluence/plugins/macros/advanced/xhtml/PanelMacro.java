package com.atlassian.confluence.plugins.macros.advanced.xhtml;

// TODOXHTML CONFDEV-1222 mode to another plugin? Original class was in com.atlassian.renderer.v2.macro.basic

public abstract class PanelMacro extends AbstractPanelMacro
{
    @Override
    protected String getPanelCSSClass()
    {
        return "panel";
    }

    @Override
    protected String getPanelContentCSSClass()
    {
        return "panelContent";
    }

    @Override
    protected String getPanelHeaderCSSClass()
    {
        return "panelHeader";
    }
}
