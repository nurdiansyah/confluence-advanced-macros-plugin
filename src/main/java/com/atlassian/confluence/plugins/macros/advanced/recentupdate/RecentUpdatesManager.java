package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.Page;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.search.v2.SearchResult;

public interface RecentUpdatesManager
{
    Page<SearchResult> find(QueryParameters queryParams, PaginationParameters paginationParams);
}
