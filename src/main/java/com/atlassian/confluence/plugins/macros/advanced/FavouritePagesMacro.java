package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FavouritePagesMacro extends BaseMacro
{
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/favpages.vm";

    private static final String SORT = "sort";

    private LabelManager labelManager;
    private PermissionManager permissionManager;

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        Map<String, Object> contextMap = getDefaultVelocityContext();

        List<ContentEntityObject> contents = new LinkedList<ContentEntityObject>();

        Integer maxResults = 5;
        try
        {
            maxResults = Integer.valueOf((String) parameters.get("maxResults"));
        }
        catch (NumberFormatException e)
        {
        }
        contextMap.put("maxResults", maxResults);


        addContentForLabelCollection(contents,"my:favourite");
        addContentForLabelCollection(contents,"my:favorite");
        
        contents = permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, contents);

        // sort the contents
        Comparator comparator = configureComparator(parameters);
        Collections.sort(contents, comparator);

        contents = filterByContentType(contents, Arrays.asList(Page.CONTENT_TYPE, BlogPost.CONTENT_TYPE));
        contextMap.put("contents", contents);

        return getRenderedTemplate(contextMap);
    }

    private void addContentForLabelCollection(Collection contents, String labelName)
    {
         ParsedLabelName ref = parseLabel(labelName);
            if (ref != null)
            {
                Label label = labelManager.getLabel(ref);
                if (label != null)
                {
                    contents.addAll(labelManager.getCurrentContentForLabel(label));
                }
            }
    }

	protected ParsedLabelName parseLabel(String labelName) 
	{
		return LabelParser.parse(labelName);
	}

    /**
     * @param original
     * @param types list of types (the ones you get from calling <ContentEntityObject>.getType())
     * @return list of ContentEntityObjects whose type matches those specified
     */
    private List<ContentEntityObject> filterByContentType(List<ContentEntityObject> original, List types)
    {
        List<ContentEntityObject> result = new ArrayList<ContentEntityObject>();

        for (ContentEntityObject contentEntityObject : original)
        {
            if (types.contains(contentEntityObject.getType()))
                result.add(contentEntityObject);
        }
        return result;
    }

    private Comparator configureComparator(Map parameters)
    {
        // null comparator... .. wait until 1.4.3 is merged into head before completing this.
        // The necessary comparator implementations are on the stable branch.
        Comparator sort = new Comparator()
        {
            public int compare(Object o1, Object o2)
            {
                return 0;
            }
        };
        if (TextUtils.stringSet((String) parameters.get(SORT)))
        {

            boolean reverse = false;
            if (TextUtils.stringSet((String) parameters.get("reverse")))
            {
                reverse = Boolean.valueOf((String) parameters.get("reverse")).booleanValue();
            }

            String algorithm = (String) parameters.get("sort");
            if ("title".equalsIgnoreCase(algorithm))
            {

            }
            else if ("modified".equalsIgnoreCase(algorithm))
            {

            }
            else if ("usage".equalsIgnoreCase(algorithm))
            {

            }

            if (reverse)
            {
//                sort = new ReversingComparator(sort);
            }
        }
        return sort;
    }

    public void setLabelManager(LabelManager manager)
    {
        this.labelManager = manager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }
    
    ///CLOVER:OFF
    protected String getRenderedTemplate(Map<String, Object> contextMap) {
        return VelocityUtils.getRenderedTemplate(TEMPLATE_NAME, contextMap);
    }

    protected Map<String, Object> getDefaultVelocityContext() {
        return MacroUtils.defaultVelocityContext();
    }
    ///CLOVER:OFF
}
