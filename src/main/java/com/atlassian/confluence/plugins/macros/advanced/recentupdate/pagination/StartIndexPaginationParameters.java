package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.ResultFilterFactory;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import org.apache.commons.lang.builder.ToStringBuilder;

public class StartIndexPaginationParameters implements PaginationParameters<Integer>, ResultFilterFactory<SubsetResultFilter>
{
    private final int startIndex;
    private final int pageSize;

    /**
     * Constructs an instance with the default page size {@link #DEFAULT_SIZE}.
     */
    public StartIndexPaginationParameters(int startIndex)
    {
        this(startIndex, DEFAULT_SIZE);
    }

    /**
     * Constructs an instance with the specified start index and page size.
     * If a page size greater than {@link #MAX_PAGE_SIZE} is specified, the size will automatically
     * be reduced to {@link #MAX_PAGE_SIZE}.
     *
     * @param pageSize must be greater than 0 and less than {@link #MAX_PAGE_SIZE}.
     */
    public StartIndexPaginationParameters(int startIndex, int pageSize)
    {
        if (startIndex < 0)
            throw new IllegalArgumentException("startIndex cannot be less than 0.");
        if (pageSize <= 0)
            throw new IllegalArgumentException("pageSize cannot be less than or equal to 0.");

        this.startIndex = startIndex;
        this.pageSize = pageSize > MAX_PAGE_SIZE ? MAX_PAGE_SIZE : pageSize;
    }

    public Integer getOffset()
    {
        return startIndex;
    }

    public String getOffSetName()
    {
        return "startIndex"; 
    }

    public SubsetResultFilter getResultFilter()
    {
        final int validatedPageSize = this.pageSize < MAX_PAGE_SIZE ? this.pageSize + 1 : MAX_PAGE_SIZE; // prohit paging beyond MAX_PAGE_SIZE 

        return new SubsetResultFilter(startIndex, validatedPageSize);
    }

    public int getPageSize()
    {
        return pageSize;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append(startIndex).append(pageSize).toString(); 
    }
}
