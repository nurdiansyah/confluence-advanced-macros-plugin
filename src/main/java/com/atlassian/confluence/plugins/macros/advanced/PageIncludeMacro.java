package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.NastyUtilitiesWhichWillBeMadeRedundant;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.deprecated.HTMLParagraphStripper;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;

import java.util.Map;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * A macro to include one Confluence page within another
 */
public class PageIncludeMacro extends BaseMacro implements Macro
{
    private PageProvider pageProvider;
    private Renderer viewRenderer;
    private I18NBeanFactory i18NBeanFactory;
    private final String RENDER_ERROR_PREFIX = "confluence.macros.advanced.include.unable-to-render";
    private final String NOT_FOUND_ERROR = "confluence.macros.advanced.include.error.content.not.found";

    private final HTMLParagraphStripper htmlParagraphStripper;

    public PageIncludeMacro()
    {
        final XMLOutputFactory xmlOutputFactory;
        try
        {
            xmlOutputFactory = (XMLOutputFactory) new XmlOutputFactoryFactoryBean(true).getObject();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error occurred trying to construct a XML output factory", e); // this shouldn't happen
        }

        htmlParagraphStripper = new HTMLParagraphStripper(xmlOutputFactory, new DefaultXmlEventReaderFactory());
    }

    public boolean isInline()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public boolean hasBody()
    {
        return false;
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            return execute(parameters, body, NastyUtilitiesWhichWillBeMadeRedundant.extractPageContext(conversionContext));
        }
        catch (MacroException e)
        {
            throw new MacroExecutionException(e);
        }
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();

        if (!(renderContext instanceof PageContext))
            return RenderUtils.blockError(i18NBean.getText(RENDER_ERROR_PREFIX), i18NBean.getText("confluence.macros.advanced.include.error.can-only-be-used-in-confluence"));

        validate(parameters);

        final PageContext pageContext = (PageContext) renderContext;
        final String location = getLocation(parameters);

        if (!TextUtils.stringSet(location))
            throw new MacroException(i18NBean.getText("confluence.macros.advanced.include.error.no.location"));

        try
        {
            ContentEntityObject page = pageProvider.resolve(location, renderContext);
            if (page == null)
            {
                return RenderUtils.blockError(i18NBean.getText(RENDER_ERROR_PREFIX), i18NBean.getText(NOT_FOUND_ERROR));
            }
            return fetchPageContent(page, pageContext);
        }
        catch (NotAuthorizedException e)
        {
            // Don't let the user know they weren't allowed to see the page.
            return RenderUtils.blockError(i18NBean.getText(RENDER_ERROR_PREFIX), i18NBean.getText(NOT_FOUND_ERROR));
        }
        catch (IllegalArgumentException e)
        {
            return RenderUtils.blockError(i18NBean.getText(RENDER_ERROR_PREFIX), e.getMessage());
        }

    }

    protected String fetchPageContent(ContentEntityObject page, PageContext pageContext)
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();

        if (ContentIncludeStack.contains(page))
            return RenderUtils.blockError(i18NBean.getText(RENDER_ERROR_PREFIX), i18NBean.getText(
                    "confluence.macros.advanced.include.error.already.included",
                    new String[]{page.getTitle()}
            ));

        ContentIncludeStack.push(page);
        try
        {
        	String strippedBody = page.getBodyAsString();
        	try
        	{
        		strippedBody = htmlParagraphStripper.stripFirstParagraph(page.getBodyAsString());
        	}
        	catch(XMLStreamException e)
        	{
        		Log.warn("Could not strip first paragraph, using unstripped body", e);
        	}

        	DefaultConversionContext context = new DefaultConversionContext(new PageContext(page, pageContext));
        	// this line of code is redundant once page include macro is built against >= CONF 4.1.2
        	context.getPageContext().setOutputType(pageContext.getOutputType());
			return viewRenderer.render(strippedBody, context);
        }
        finally
        {
            ContentIncludeStack.pop();
        }
    }

    String getLocation(Map parameters)
    {
        final String spaceKey = TextUtils.noNull((String) parameters.get("spaceKey")).trim();
        final String pageTitle = GeneralUtil.unescapeEntities(TextUtils.noNull((String) parameters.get("pageTitle")).trim());
        final String location = GeneralUtil.unescapeEntities(TextUtils.noNull((String) parameters.get("0")).trim());

        if (StringUtils.isBlank(pageTitle))
            return location;
        
        return toPageLink(spaceKey, pageTitle);
    }

    void validate(Map parameters) throws MacroException
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();

        final String pageTitle = (String) parameters.get("pageTitle");
        final String location = (String) parameters.get("0");

        if (StringUtils.isBlank(location))
        {
            if (StringUtils.isBlank(pageTitle))
                throw new MacroException(i18NBean.getText("confluence.macros.advanced.include.error.no.page-title"));
        }
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean();
    }

    String toPageLink(String space, String pageTitle)
    {
        return (StringUtils.isBlank(space))
                ? pageTitle
                : space + ":" + pageTitle;
    }

    public void setViewRenderer(Renderer viewRenderer)
    {
        this.viewRenderer = viewRenderer;
    }

    public void setPageProvider(PageProvider pageProvider)
    {
        this.pageProvider = pageProvider;
    }

    public void setUserI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }
}
