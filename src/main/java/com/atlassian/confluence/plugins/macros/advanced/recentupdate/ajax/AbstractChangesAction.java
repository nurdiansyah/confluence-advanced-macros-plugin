package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.ChangesUrl;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.DefaultRecentUpdatesManager;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.DefaultUpdateItemFactory;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.QueryParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.RecentUpdatesManager;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.UpdateItem;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.Page;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartHandlePaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartIndexPaginationParameters;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractChangesAction extends ConfluenceActionSupport
{
    private SearchManager searchManager;
    private AnyTypeDao anyTypeDao;
    private String authors;
    private int pageSize = PaginationParameters.DEFAULT_SIZE;
    private String contentType;
    private String spaceKeys;
    private String labels;

    private ContentTypesDisplayMapper contentTypesDisplayMapper;

    /**
     * Flag to allow us to indicate whether or not this action is serving the first page of results
     */
    private boolean servingFirstPageOfResults = true;
    /**
     * The string representation of {@link com.atlassian.bonnie.Handle} of the first result on the next page of results
     */
    private String startHandle;
    private int startIndex = 0;
    private ChangesUrl nextPageUrl;

    List<UpdateItem> updateItems;

    @Override public void validate()
    {
        super.validate();

        if (StringUtils.isNotBlank(startHandle))
        {
            try
            {
                Handle handle = new HibernateHandle(startHandle);
                if (anyTypeDao.findByHandle(handle) == null)
                    addActionError(getText("recently.updated.more.results.error"));
            }
            catch (ParseException e)
            {
                addActionError("Badly formatted handle: " + startHandle);
            }
        }
    }

    @Override public String execute() throws Exception
    {
        PaginationParameters paginationParameters;
        if (StringUtils.isNotBlank(startHandle))
        {
            servingFirstPageOfResults = false; // start handle based will never be applied to retrieve the first of page of results
            paginationParameters = new StartHandlePaginationParameters(new HibernateHandle(startHandle), pageSize);
        }
        else
            paginationParameters = new StartIndexPaginationParameters(startIndex, pageSize);

        QueryParameters queryParameters = new QueryParameters(labels, authors, contentType, spaceKeys);
        RecentUpdatesManager recentUpdatesManager = new DefaultRecentUpdatesManager(searchManager);

        final DefaultUpdateItemFactory updateItemFactory = new DefaultUpdateItemFactory(getDateFormatter(), getI18n(), contentTypesDisplayMapper);

        final Page<SearchResult> pageOfResults = recentUpdatesManager.find(queryParameters, paginationParameters);

        if (pageOfResults.morePages())
            nextPageUrl = new ChangesUrl(queryParameters, pageOfResults.getNextPageParameters(), getTheme());

        updateItems = new ArrayList<UpdateItem>(pageOfResults.size());
        for (SearchResult searchResult : pageOfResults.getItems())
        {
            UpdateItem updateItem = updateItemFactory.get(searchResult);
            if (updateItem != null)
                updateItems.add(updateItem);
        }

        return super.execute();
    }

    protected abstract Theme getTheme();

    public void setAuthors(String authors)
    {
        this.authors = authors;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public void setSearchManager(SearchManager searchManager)
    {
        this.searchManager = searchManager;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public void setSpaceKeys(String spaceKeys)
    {
        this.spaceKeys = spaceKeys;
    }

    public void setLabels(String labels)
    {
        this.labels = labels;
    }

    public void setStartHandle(String startHandle)
    {
        this.startHandle = startHandle;
    }

    public ChangesUrl getNextPageUrl()
    {
        return nextPageUrl;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public void setAnyTypeDao(AnyTypeDao anyTypeDao)
    {
        this.anyTypeDao = anyTypeDao;
    }

    public boolean isServingFirstPageOfResults()
    {
        return servingFirstPageOfResults;
    }

    public List<UpdateItem> getUpdateItems()
    {
        return updateItems;
    }

    public void setContentTypesDisplayMapper(ContentTypesDisplayMapper contentTypesDisplayMapper)
    {
        this.contentTypesDisplayMapper = contentTypesDisplayMapper;
    }

}
