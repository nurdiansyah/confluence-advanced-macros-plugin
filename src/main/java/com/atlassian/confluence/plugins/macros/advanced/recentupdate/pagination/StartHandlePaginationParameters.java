package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.ResultFilterFactory;
import com.atlassian.confluence.search.v2.filter.HandleBasedSubsetResultFilter;

public class StartHandlePaginationParameters implements PaginationParameters<Handle>, ResultFilterFactory<HandleBasedSubsetResultFilter>
{
    private final Handle handle;
    private final int pageSize;

    /**
     * Constructs an instance with the default page size {@link #DEFAULT_SIZE}.
     */
    public StartHandlePaginationParameters(Handle handle)
    {
        this(handle, DEFAULT_SIZE);
    }

    /**
     * Constructs an instance with the specified start handle and page size.
     */
    public StartHandlePaginationParameters(Handle handle, int pageSize)
    {
        if (handle == null)
            throw new IllegalArgumentException("Handle is required."); 
        if (pageSize <= 0)
            throw new IllegalArgumentException(pageSize + " cannot be less than or equal to 0.");

        this.handle = handle;
        this.pageSize = pageSize;
    }

    /**
     * Returns a result filter that fetches one more result than the page size to allow clients to determine if more pages exist.
     *
     *
     * @return a result filter that fetches one more result than the page size to allow clients to determine if more pages exist
     */
    public HandleBasedSubsetResultFilter getResultFilter()
    {
        return new HandleBasedSubsetResultFilter(handle, this.pageSize + 1);
    }

    public Handle getOffset()
    {
        return handle;
    }

    public String getOffSetName()
    {
        return "startHandle"; 
    }

    public int getPageSize()
    {
        return pageSize;
    }
}
