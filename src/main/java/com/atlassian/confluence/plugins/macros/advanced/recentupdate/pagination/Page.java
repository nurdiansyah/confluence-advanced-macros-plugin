package com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination;

import java.util.Collections;
import java.util.List;

public class Page<T>
{
    private static final Page EMPTY_PAGE = new Page(Collections.emptyList(), PaginationParameters.DEFAULT_SIZE);

    private final List<T> items;
    private final int pageSize;
    private PaginationParameters nextPageParameters;

    public Page(List<T> items, int pageSize)
    {
        if (pageSize < 0)
            throw new IllegalArgumentException("pageSize must be greater than 0.");

        this.items = items;
        this.pageSize = pageSize;
    }

    public List<T> getItems()
    {
        if (morePages())
            return items.subList(0, pageSize);
        
        return items;
    }

    /**
     * @return the page size
     */
    public int size()
    {
        return pageSize;
    }

    /**
     * @return true if there are more results after this page
     */
    public boolean morePages()
    {
        return items.size() > pageSize; 
    }

    public static <T> Page<T> emptyPage()
    {
        return (Page<T>) EMPTY_PAGE;
    }

    public void setNextPageParameters(PaginationParameters nextPageParameters)
    {
        this.nextPageParameters = nextPageParameters;
    }

    public PaginationParameters getNextPageParameters()
    {
        if (!morePages())
            throw new IllegalStateException("There are no more results after this page.");

        return nextPageParameters;
    }
}
